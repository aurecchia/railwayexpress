package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.controllers.TrainSelectionController;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Displays the list of trains owned by the player.
 * 
 */
public class TrainListPanel extends LayoutPanel {
    
    /**
     * The ratio of the panel.
     */
    private static final float PANEL_RATIO = 3.55556f;
    
    /**
     * Create a new train list panel with the given dimensions and with inner components staked vertically, without
     * padding.
     * 
     * @param rectangle
     *            The dimension and position of the panel.
     * @param trainSelectionController
     *            train selection controller.
     */
    public TrainListPanel(final Rectangle rectangle, final TrainSelectionController trainSelectionController) {
        super(rectangle);
        trainSelectionController.initTrainList(this);
    }
    
    /**
     * Add a new panel for the given train.
     * 
     * @param train
     *            The train for which to add a panel.
     * @return The panel created for the train.
     */
    public final TrainStatusPanel addNewTrainPanel(final Train train) {
        final TrainStatusPanel newPanel = new TrainStatusPanel(new Rectangle(SystemConstants.ZERO,
            SystemConstants.ZERO, this.getWidth(), this.getWidth() / TrainListPanel.PANEL_RATIO), train);
        this.add(newPanel);
        newPanel.select();
        return newPanel;
    }
    
}
