package ch.usi.inf.saiv.railwayempire.model.structures;

/**
 * Defines the behavior of a observer for the station manager.
 * 
 */
public interface IStationManagerObserver {
    
    /**
     * Notifies the observer that the given station was just added.
     * 
     * @param station
     *            The station which was added.
     */
    void notifyStationAdded(Station station);
    
    /**
     * Notifies the observer that the given station was just removed.
     * 
     * @param station
     *            The station which was removed.
     */
    void notifyStationRemoved(Station station);
    
    /**
     * Notifies the observer that the given station was just linked.
     * 
     * @param station
     *            The station which was linked.
     */
    void notifyStationLinked(Station station);
    
    /**
     * Notifies the observer that the given station was just unlinked.
     * 
     * @param station
     *            The station which was unlinked.
     */
    void notifyStationUnlinked(Station station);
    
}
