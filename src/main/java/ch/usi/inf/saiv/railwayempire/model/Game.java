package ch.usi.inf.saiv.railwayempire.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Time;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;


/**
 * Game class.
 */
public final class Game implements Serializable {

    /**
     * serialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = 6480921296023350000L;
    /**
     * The unique instance of the game.
     */
    private static Game uniqueInstance = new Game();
    /**
     * List of objects to notify when we load new game.
     */
    private static List<IPersistent> persistents = new ArrayList<IPersistent>();
    /**
     * The year the game has to start from.
     */
    private final long startYear;
    /**
     * The instance of the player.
     */
    private final Player player;
    /**
     * The world in this game.
     */
    private World world;
    /**
     * Time instance.
     */
    private Time time;

    /**
     * Creates a new game.
     */
    private Game() {
        this.startYear = GameConstants.START_YEAR;
        this.player = new Player();
        this.time = Time.getInstance();
    }

    /**
     * Returns the unique instance of the game.
     *
     * @return The instance of the game.
     */
    public static Game getInstance() {
        return Game.uniqueInstance;
    }

    /**
     * Add persistent to the list of persistent elements.
     *
     * @param persistent
     *         persistent.
     */
    public static void addPersistent(final IPersistent persistent) {
        if (persistent != null) {
            Game.persistents.add(persistent);
        }
    }

    /**
     * Return the player.
     *
     * @return The player.
     */
    public Player getPlayer() {
        return this.player;
    }

    /**
     * Generates world.
     *
     * @param settings
     *            parameters for terrain generation.
     */
    public void generateWorld(final WorldSettings settings) {
        this.world = new World(settings);
    }

    /**
     * Returns the world of the game.
     *
     * @return The world.
     */
    public World getWorld() {
        return this.world;
    }

    /**
     * Starts the game.
     */
    public void startGame() {
        Time.getInstance().setStartingYear(this.startYear);
    }

    /**
     * Resumes the state of a loaded game.
     *
     * @param loadedGame
     *         The loaded game.
     */
    public void loadGame(final Game loadedGame) {
        Game.uniqueInstance = loadedGame;
        Time.getInstance().setTime(Game.getInstance().getTime().getMilliseconds());
        Time.getInstance().setEveryframeObservers(loadedGame.getTime().getEveryframeObservers());
        Time.getInstance().setEveryMinuteElapsedTime(loadedGame.getTime().getEveryMinuteObservers());
        for (final IPersistent persistent : Game.persistents) {
            if (persistent != SystemConstants.NULL) {
                persistent.reload();
            }
        }
        CoordinatesManager.getInstance().centerCameraOnTracks();
    }

    /**
     * Getter for the time.
     *
     * @return time.
     */
    public Time getTime() {
        return this.time;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.getPlayer().getPlayerName() + "  " + this.getWorld().toString();
    }
}
