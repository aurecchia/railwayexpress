package ch.usi.inf.saiv.railwayempire.utilities;


import java.io.Serializable;


/**
 * Represents a coordinate in a 2D space.
 */
public final class Coordinate implements Serializable {

    /**
     * SerialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = -3371463175455555756L;
    /**
     * The x coordinate.
     */
    private final int posX;
    /**
     * The y coordinate.
     */
    private final int posY;

    /**
     * Constructor for the coordinate.
     *
     * @param newX
     *         The x position.
     * @param newY
     *         The y position.
     */
    public Coordinate(final int newX, final int newY) {
        this.posX = newX;
        this.posY = newY;
    }

    /**
     * Returns the x coordinate.
     *
     * @return The x coordinate.
     */
    public int getX() {
        return this.posX;
    }

    /**
     * Returns the y coordinate.
     *
     * @return The y coordinate.
     */
    public int getY() {
        return this.posY;
    }

    /**
     * Returns the hascode for the coordinate.
     *
     * @return The hashcode.
     */
    @Override
    public int hashCode() {
        return 31 * this.getX() + this.getY();
    }

    /**
     * Tells whether the two objects are equal.
     *
     * @param other
     *         The other object
     * @return True if the two object are equal, false otherwise.
     */
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }

        final Coordinate that = (Coordinate) other;

        if (this.getX() != that.getX() || this.getY() != that.getY()) {
            return false;
        }

        return true;
    }
}
