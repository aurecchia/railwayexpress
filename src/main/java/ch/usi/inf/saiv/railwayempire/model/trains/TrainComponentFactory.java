package ch.usi.inf.saiv.railwayempire.model.trains;


import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.newdawn.slick.util.ResourceLoader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.usi.inf.saiv.railwayempire.model.wagons.CoalWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.FoodWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.IronWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.MailWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.OilWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.PassengersWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.RockWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.WoodWagon;
import ch.usi.inf.saiv.railwayempire.utilities.Log;


/**
 * Make trains.
 */
public final class TrainComponentFactory {

    /**
     * Singleton pattern.
     */
    private static final TrainComponentFactory UNIQUE_INSTANCE = new TrainComponentFactory();
    /**
     * locomotive map.
     */
    private final Map<String, Locomotive> locomotiveMap;
    /**
     * wagon map.
     */
    private final Map<String, IWagon> wagonMap;

    /**
     * Default empty constructor.
     */
    private TrainComponentFactory() {
        this.locomotiveMap = new HashMap<String, Locomotive>();
        this.wagonMap = new HashMap<String, IWagon>();

        InputStream inputStream = null;
        try {
            inputStream = ResourceLoader.getResource("xml/traincomponents.xml").openStream();
        } catch (final IOException exception) {
            Log.exception(exception);
        }

        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (final ParserConfigurationException exception) {
            Log.exception(exception);
        }

        Document document = null;
        try {
            document = documentBuilder.parse(inputStream);
        } catch (final SAXException exception) {
            Log.exception(exception);
        } catch (final IOException exception) {
            Log.exception(exception);
        }

        document.getDocumentElement().normalize();

        final NodeList locomotiveNodeList = document.getElementsByTagName("locomotive");
        for (int resourceIndex = 0; resourceIndex < locomotiveNodeList.getLength(); ++resourceIndex) {
            final Node resourceNode = locomotiveNodeList.item(resourceIndex);
            if (resourceNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element resourceElement = (Element) resourceNode;
                final String name = resourceElement.getTextContent();
                final int cost = Integer.parseInt(resourceElement.getAttribute("cost"));
                final float velocity = Float.parseFloat(resourceElement.getAttribute("velocity"));
                final int maxWagons = Integer.parseInt(resourceElement.getAttribute("maxWagons"));
                final String texture = resourceElement.getAttribute("texture");
                this.locomotiveMap.put(name, new Locomotive(name, cost, velocity, maxWagons, texture));
            }
        }

        final NodeList wagonNodeList = document.getElementsByTagName("wagon");
        for (int resourceIndex = 0; resourceIndex < wagonNodeList.getLength(); ++resourceIndex) {
            final Node resourceNode = wagonNodeList.item(resourceIndex);
            if (resourceNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element resourceElement = (Element) resourceNode;
                final String type = resourceElement.getAttribute("type");
                final String name = resourceElement.getTextContent();
                final int cost = Integer.parseInt(resourceElement.getAttribute("cost"));
                final int unitLimit = Integer.parseInt(resourceElement.getAttribute("limit"));
                final double loadTime = Double.parseDouble(resourceElement.getAttribute("loadTime"));
                if ("mail".equals(type)) {
                    this.wagonMap.put(name, new MailWagon(name, cost, unitLimit, loadTime));
                } else if ("passenger".equals(type)) {
                    this.wagonMap.put(name, new PassengersWagon(name, cost, unitLimit, loadTime));
                } else if ("coal".equals(type)) {
                    this.wagonMap.put(name, new CoalWagon(name, cost, unitLimit, loadTime));
                } else if ("food".equals(type)) {
                    this.wagonMap.put(name, new FoodWagon(name, cost, unitLimit, loadTime));
                } else if ("iron".equals(type)) {
                    this.wagonMap.put(name, new IronWagon(name, cost, unitLimit, loadTime));
                } else if ("oil".equals(type)) {
                    this.wagonMap.put(name, new OilWagon(name, cost, unitLimit, loadTime));
                } else if ("rock".equals(type)) {
                    this.wagonMap.put(name, new RockWagon(name, cost, unitLimit, loadTime));
                } else if ("wood".equals(type)) {
                    this.wagonMap.put(name, new WoodWagon(name, cost, unitLimit, loadTime));
                } else {
                    Log.error("Trying to load unknown wagon type: " + name);
                }
                // TODO: add more wagons.
            }
        }
    }

    /**
     * Return the unique instance.
     *
     * @return the instance.
     */
    public static TrainComponentFactory getInstance() {
        return UNIQUE_INSTANCE;
    }

    /**
     * Return the locomotive with the given name.
     *
     * @param name
     *         the name of the locomotive.
     * @return the locomotive.
     */
    public Locomotive getLocomotive(final String name) {
        return this.locomotiveMap.get(name).copy();
    }

    /**
     * Return the wagon with the given name.
     *
     * @param name
     *         the name of the wagon.
     * @return the wagon.
     */
    public IWagon getWagon(final String name) {
        return this.wagonMap.get(name).copy();
    }

    /**
     * Returns a collection of all loaded locomotives.
     *
     * @return The locomotives.
     */
    public Collection<Locomotive> getLocomotives() {
        return this.locomotiveMap.values();
    }

    /**
     * Returns a collection of all loaded wagons.
     *
     * @return The wagons.
     */
    public Collection<IWagon> getWagons() {
        return this.wagonMap.values();
    }
}
