package ch.usi.inf.saiv.railwayempire.model.generators;


import java.util.Random;

import org.newdawn.slick.geom.Vector2f;

import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.GroundCell;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.cells.MountainCell;
import ch.usi.inf.saiv.railwayempire.model.cells.RiverCell;
import ch.usi.inf.saiv.railwayempire.model.cells.SeaCell;
import ch.usi.inf.saiv.railwayempire.model.structures.Forest;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Noise;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;


/**
 * The terrain generator responsible for the creation of a realistic terrain
 * and
 * the creation of the cells contained in it.
 */
public final class TerrainGenerator {

    /**
     * Empty constructor for the terrain generator.
     */
    private TerrainGenerator() {
    }

    /**
     * Basic implementation of terrain generation. Creates a fractal noise and
     * a terrain based on the values of the noise interpreted as a height-map.
     *
     * @param settings
     *         Affects how the terrain is generated.
     * @return The generated terrain.
     */
    public static Terrain generateTerrain(final WorldSettings settings) {
        final Noise noise = new Noise(GameConstants.WORLD_SIZE);
        noise.generateFractalNoise(settings.getOctaves(), settings.getPersistence());
        final Terrain terrain = new Terrain(noise.getSize(), noise.getSize());

        for (int posX = 0; posX < noise.getSize(); ++posX) {
            for (int posY = 0; posY < noise.getSize(); ++posY) {
                if (noise.getValue(posX, posY) <= GameConstants.WATER_LEVEL) {
                    final int height = (int) ((noise.getValue(posX, posY) * 9) / (GameConstants.WATER_LEVEL));
                    terrain.addCell(posX, posY, new SeaCell(height));
                } else {
                    final int height = (int) (((noise.getValue(posX, posY) - GameConstants.WATER_LEVEL) * 9) / (1
                            - GameConstants.WATER_LEVEL));
                    terrain.addCell(posX, posY, new GroundCell(height));
                }
            }
        }
        for (int i = 0; i < settings.getRiversNumber(); ++i) {
            TerrainGenerator.generateRiver(terrain, noise);
        }

        TerrainGenerator.generateMountains(terrain, noise);
        TerrainGenerator.generateForest(terrain);
        return terrain;
    }

    /**
     * Method which takes a terrain and randomly places groups of different forest types.
     * Red type of forest is considered as more rare than the rest forest types, thats why
     * it has to be generated to a certain minimum of roughly 5%.
     *
     * @param terrain
     *         The terrain.
     */
    private static void generateForest(final Terrain terrain) {
        final Random random = new Random();
        final Noise noise = new Noise(GameConstants.WORLD_SIZE);
        final int constant = 10;
        int typePercentage = 0;
        noise.generateFractalNoise(5, 0.4);
        for (int posX = 0; posX < terrain.getSize(); ++posX) {
            for (int posY = 0; posY < terrain.getSize(); ++posY) {
                int forestType = random.nextInt(GameConstants.FOREST_TEXTURES_NUMBER);
                final ICell currentCell = terrain.getCell(posX, posY);
                if (currentCell.getCellType() == CellTypes.GROUND
                        && 1 <= noise.getValue(posX, posY) * constant
                        && 5 >= noise.getValue(posX, posY) * constant
                        && !currentCell.containsStructure()) {
                    typePercentage++;
                    if (typePercentage > constant && forestType != SystemConstants.ONE) {
                        forestType = 0;
                        typePercentage = 0;
                    }
                    if (noise.getValue(posX, posY) > 0.35 && forestType == 1) {
                        forestType = GameConstants.FOREST_TEXTURES_NUMBER - SystemConstants.ONE;
                    }
                    currentCell.setStructure(new Forest(posX, posY, forestType));
                }
            }
        }
    }

    /**
     * Method which takes a terrain and places mountains on highest cells.
     *
     * @param terrain
     *         The terrain.
     * @param noise
     *         The noise used to generate the terrain.
     */
    private static void generateMountains(final Terrain terrain, final Noise noise) {
        for (int posX = 0; posX < noise.getSize(); ++posX) {
            for (int posY = 0; posY < noise.getSize(); ++posY) {
                final ICell currentCell = terrain.getCell(posX, posY);
                if (currentCell.getCellType() == CellTypes.GROUND) {
                    final int value = currentCell.getHeight();
                    if (value >= 6) {
                        terrain.setCell(posX, posY, new MountainCell(6));
                    } else if (value >= 5) {
                        terrain.setCell(posX, posY, new MountainCell(5));
                    }
                }
            }
        }
    }

    /**
     * Generate a river in the given terrain.
     *
     * @param terrain
     *         The terrain.
     * @param noise
     *         The noise used to generate the terrain.
     */
    private static void generateRiver(final Terrain terrain, final Noise noise) {
        Vector2f currentPos = new Vector2f();
        ICell currentCell = null;

        // 1. find starting cell.
        final Random random = new Random();
        int deadlock = 0;
        while (currentCell == SystemConstants.NULL && deadlock++ < 1000) {
            currentPos.set(random.nextInt(terrain.getSize()), random.nextInt(terrain.getSize()));
            final ICell cell = terrain.getCell((int) currentPos.getX(), (int) currentPos.getY());
            if (cell.getCellType() == CellTypes.GROUND && cell.getHeight() > 5) {
                currentCell = cell;
            }
        }

        // flow to the sea.
        if (null != currentCell) {
            while (currentCell.getCellType() != CellTypes.SEA) {

                terrain.setCell((int) currentPos.getX(), (int) currentPos.getY(),
                        new RiverCell(currentCell.getHeight()));

                currentPos = TerrainGenerator.getLowestNeighbor(terrain, noise, currentPos, SystemConstants.ONE);
                if (currentPos == SystemConstants.NULL) {
                    return;
                } else {
                    currentCell = terrain.getCell((int) currentPos.getX(), (int) currentPos.getY());
                }
            }
        }
    }

    /**
     * Returns a coordinates of neighbor that are on the lowest height level. Also the neighbors must be ground cells.
     * Returns Null if sea is found.
     *
     * @param terrain
     *         The terrain.
     * @param noise
     *         The noise used to generate the terrain.
     * @param center
     *         The coordinates of the cell we are considering.
     * @return Coordinate
     */
    private static Vector2f getLowestNeighbor(final Terrain terrain, final Noise noise, final Vector2f center,
                                              final int distance) {

        final Vector2f retCoords = new Vector2f();

        final int posX = (int) center.getX();
        final int posY = (int) center.getY();

        double height = 9000; // nothing will be bigger than that.
        for (int y = posY - distance; y <= posY + distance; ++y) {
            for (int x = posX - distance; x <= posX + distance; ++x) {
                if (Math.abs(x - posX) + Math.abs(y - posY) == distance) {
                    final ICell cell = terrain.getCell(x, y);
                    if (cell.getCellType() == CellTypes.SEA) {
                        return null;
                    } else if (cell.getCellType().equals(CellTypes.GROUND)) {
                        final double newHeight = noise.getValue(x, y);
                        if (newHeight < height) {
                            height = newHeight;
                            retCoords.set(x, y);
                        }
                    }
                }
            }
        }
        return retCoords;
    }
}
