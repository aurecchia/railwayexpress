package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Shape;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;

/**
 * Represent the central part of the bottom panel.
 */
public abstract class AbstractInfoPanel extends AbstractPanel {
    
    /**
     * Constructor.
     * 
     * @param shape
     *            Some shape apparently.
     */
    protected AbstractInfoPanel(final Shape shape) {
        super(shape);
    }
}
