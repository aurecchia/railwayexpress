package ch.usi.inf.saiv.railwayempire.model.stores;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainComponentFactory;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;

/**
 * A representation of the store for train components.
 */
public final class TrainComponentStore {
    
    /**
     * Instance of the store.
     */
    private static final TrainComponentStore UNIQUE_INSTANCE = new TrainComponentStore();
    /**
     * List of the purchasable locomotives.
     */
    private final List<Locomotive> purchasableLocomotives;
    /**
     * List of the purchasable wagons.
     */
    private final List<IWagon> purchasableWagons;

    /**
     * Creates an instance of the store.
     */
    private TrainComponentStore() {
        this.purchasableLocomotives = new ArrayList<Locomotive>();
        this.purchasableWagons = new ArrayList<IWagon>();
        for (final Locomotive locomotive : TrainComponentFactory.getInstance().getLocomotives()) {
            this.purchasableLocomotives.add(locomotive);
        }
        
        for (final IWagon wagon : TrainComponentFactory.getInstance().getWagons()) {
            this.purchasableWagons.add(wagon);
        }
    }
    
    /**
     * Getter.
     * 
     * @return game instance.
     */
    public static TrainComponentStore getUniqueInstance() {
        return TrainComponentStore.UNIQUE_INSTANCE;
    }
    
    /**
     * Getter.
     * 
     * @return list of available locomotives
     */
    public List<Locomotive> getPurchasableLocomotives() {
        return this.purchasableLocomotives;
    }
    
    /**
     * Getter.
     * 
     * @return list of available wagons.
     */
    public List<IWagon> getPurchasableWagons() {
        return this.purchasableWagons;
    }

}
