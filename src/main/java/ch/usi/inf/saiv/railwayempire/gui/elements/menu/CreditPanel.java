package ch.usi.inf.saiv.railwayempire.gui.elements.menu;


import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.MainMenuController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;


/**
 * Panel displaying the credits of the game.
 */
public class CreditPanel extends SimplePanel {
    
    /**
     * The color to fade the screen.
     */
    private static final Color FADE_COLOR = new Color(0, 0, 0, 150);
    /**
     * The background color of the panel.
     */
    private static final Color BACKGROUND_COLOR = new Color(57, 51, 46);
    /**
     * The width of a button.
     */
    private static final int BUTTON_WIDTH = ResourcesLoader.getInstance().getImage("BACK_BUTTON").getWidth();
    /**
     * The width of the panel.
     */
    private static final int PANEL_WIDTH = 500;
    /**
     * The height of the panel.
     */
    private static final int PANEL_HEIGHT = 400;
    /**
     * The radius of the corner of the panel.
     */
    private static final int PANEL_RADIUS = 15;
    /**
     * The height of each credits block.
     */
    private static final int BLOCK_HEIGHT = 50;
    /**
     * The margin between each block.
     */
    private static final int BLOCK_MARGIN = 10;
    /**
     * The bigger font used for titles.
     */
    private static final UnicodeFont BIG_FONT = FontUtils.resizeFont(FontUtils.FONT, 20);
    
    /**
     * Instantiate a new credits panel.
     *
     * @param newShape
     *         The shape of the panel.
     * @param controller
     *         the controller handling this panel.
     */
    public CreditPanel(final Shape newShape, final MainMenuController controller) {
        super(newShape);
        
        final Button cancelButton = new Button(
            ResourcesLoader.getInstance().getImage("BACK_BUTTON"),
            ResourcesLoader.getInstance().getImage("BACK_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("BACK_BUTTON_PRESSED"),
            this.getWidth() / 2 + CreditPanel.PANEL_WIDTH / 2 - CreditPanel.BUTTON_WIDTH,
            this.getHeight() / 2 + CreditPanel.PANEL_HEIGHT / 2 + CreditPanel.BLOCK_MARGIN
            );
        
        cancelButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                controller.setMainPanel();
            }
        });
        
        this.add(cancelButton);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setColor(CreditPanel.FADE_COLOR);
        graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        
        final int panelX = this.getWidth() / 2 - CreditPanel.PANEL_WIDTH / 2;
        final int panelY = this.getHeight() / 2 - CreditPanel.PANEL_HEIGHT / 2;
        
        graphics.setColor(CreditPanel.BACKGROUND_COLOR);
        graphics.fillRoundRect(panelX, panelY, CreditPanel.PANEL_WIDTH, CreditPanel.PANEL_HEIGHT,
            CreditPanel.PANEL_RADIUS);
        
        graphics.setColor(Color.black);
        graphics.drawRoundRect(panelX, panelY, CreditPanel.PANEL_WIDTH - 1, CreditPanel.PANEL_HEIGHT - 1,
            CreditPanel.PANEL_RADIUS);
        
        int blockY = panelY + 2 * CreditPanel.BLOCK_MARGIN;
        
        blockY += this.drawBigCreditBlock(panelX, blockY, "Developed by");
        blockY += this.drawCreditBlock(panelX, blockY, "Aurecchia", "Alessio", "AVATAR_AURI", true);
        blockY += this.drawCreditBlock(panelX, blockY, "Azara", "Antonio", "AVATAR_ANTO", false);
        blockY += this.drawCreditBlock(panelX, blockY, "Dafflon", "Jacques", "AVATAR_JACQUES", true);
        blockY += this.drawCreditBlock(panelX, blockY, "Gori", "Giorgio", "AVATAR_JOJO", false);
        blockY += this.drawCreditBlock(panelX, blockY, "Gricuks", "Daneks", "AVATAR_DANEKS", true);
        blockY += this.drawCreditBlock(panelX, blockY, "Viviani", "Giovanni", "AVATAR_GIO", false);
        blockY += this.drawBigCreditBlock(panelX, blockY, "Using");
        blockY += this.drawCreditBlock(panelX, blockY, "The Slick2D graphical library.",
            "(http://www.slick2d.org/)");
        blockY += this.drawCreditBlock(panelX, blockY, "Some of the graphics from the OpenTTD project.",
            "(http://www.openttd.org/en/)");
        
        super.render(container, graphics);
    }
    
    /**
     * Draws a block of credits with the bug font.
     *
     * @param posX
     *         The x coordinate where to draw.
     * @param posY
     *         The y coordinate where to draw.
     * @param text
     *         The text to draw.
     * @return The increase in vertical offset.
     */
    private int drawBigCreditBlock(final int posX, final int posY, final String text) {
        FontUtils.drawCenter(CreditPanel.BIG_FONT, text, posX, posY, CreditPanel.PANEL_WIDTH);
        return CreditPanel.BLOCK_HEIGHT;
    }
    
    /**
     * Draws a credit block composed by two lines.
     *
     * @param posX
     *         The x coordinate where to draw.
     * @param posY
     *         the y coordinate where to draw.
     * @param text
     *         The string to draw.
     * @param second
     *         The second string to draw.
     * @return The increase in vertical offset.
     */
    private int drawCreditBlock(final int posX, final int posY, final String text, final String second) {
        FontUtils.drawCenter(FontUtils.FONT, text, posX, posY, CreditPanel.PANEL_WIDTH);
        FontUtils.drawCenter(FontUtils.FONT, second, posX, posY + 16, CreditPanel.PANEL_WIDTH);
        return CreditPanel.BLOCK_HEIGHT;
    }
    
    /**
     * Draws a credit block with picture, placed either in the left or right half of the panel based on the given
     * parameter.
     *
     * @param posX
     *         The x coordinate where to draw.
     * @param posY
     *         The y coordinate where to draw.
     * @param firstLine
     *         The string to draw on the first line.
     * @param secondLine
     *         The string to draw on the second line.
     * @param imageId
     *         The id of the image to draw.
     * @param left
     *         A boolean value telling to place the block in the left or right part of the panel.
     * @return The in crease in vertical offset.
     */
    private int drawCreditBlock(final int posX, final int posY, final String firstLine, final String secondLine,
        final String imageId, final boolean left) {
        int middle = posX + CreditPanel.PANEL_WIDTH / 2;
        if (left) {
            middle -= CreditPanel.PANEL_WIDTH / 4;
        } else {
            middle += CreditPanel.PANEL_WIDTH / 4;
        }
        
        final Image avatar = ResourcesLoader.getInstance().getImage(imageId);
        final int avatarX = middle - avatar.getWidth() - 4;
        avatar.draw(avatarX, posY - 6, 0.8f);
        
        final int textX = middle + 4;
        FontUtils.drawLeft(FontUtils.FONT, firstLine, textX, posY);
        FontUtils.drawLeft(FontUtils.FONT, secondLine, textX, posY + 16);
        
        if (left) {
            return 0;
        } else {
            return CreditPanel.BLOCK_HEIGHT + CreditPanel.BLOCK_MARGIN;
        }
    }
}
