/**
 * Provides application with different utilities in order to do a specific operation otherwise not possible.
 *
 * Coordinate represents a coordinate in a 2D space.
 *
 * FileUtils is a class that allows the application to read and write to files.
 *
 * FontUtils is a class that provides the application with justified font.
 *
 * GameConstants is a class with 0 methods. It is used to define global,static constants related to game used
 * in different places of the application in order to avoid magic numbers.
 *
 * GraphicsUtils is a support class for viewers. It provides some additional graphical enhancements.
 *
 * IPersistent is a support interface class for saving and loading classes. If a class is implementing this interface, it is
 * possible to save the state of it and thus also being able to reload it whenever asked.
 *
 * Log is a custom log which provides the developer with the option to output information in the screen log which can be seen
 * in game by the player or output the information to the file for latter use.
 *
 * Noise is a support class for terrain generator. It provides terrain generator with the plasma noise in 2D array which
 * the can be used to assign the type of each cell in our 2D matrix.
 *
 * Pair represents an ordered pair. It is used for train drawing.
 *
 * ResourcesLoader takes care of loading every image being used by the application. The images are read from an xml file. Given the ID,
 * ResourcesLoader return the image. It also takes care of loading sound, colors and train component specification.
 *
 * StatisticsManager manages all the statistics.
 *
 * SystemConstants is a class with 0 methods. It is used to define global,static constants related to system used
 * in different places of the application in order to avoid magic numbers.
 *
 * Time provides the application with custom time(fake,virtual time). The date and time is different that of the real one.
 *
 * TimeObserver is a design patter that allows different classes perform action to game time.
 *
 * TrainStatistics takes care of train statistics.
 *
 * WorldSettings class that can be used for passing different parameters to generators.
 */
package ch.usi.inf.saiv.railwayempire.utilities;