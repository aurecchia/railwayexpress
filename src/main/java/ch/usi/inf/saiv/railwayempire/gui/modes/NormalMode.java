package ch.usi.inf.saiv.railwayempire.gui.modes;

import java.util.Collection;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.structures.IStructure;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.model.structures.Track;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Time;

/**
 * Mode to handle user input and visualization of normal gameplay.
 */
public final class NormalMode extends AbstractGuiComponent implements IMode {
    /**
     * Number of frames to wait before the box appears.
     */
    private static final int BOX_FRAMES_WAITING_TIME = 20;
    /**
     * Box corner radius.
     */
    private static final int BOX_CORNER_RADIUS = 5;
    /**
     * Padding of the box.
     */
    private static final int BOX_PADDING = 10;
    /**
     * Background color of the box.
     */
    private static final Color BOX_COLOR = new Color(0, 0, 0, .5f);
    /**
     * Cell we are hovering on.
     */
    private ICell hoverCell;
    /**
     * Counter to know how much to wait.
     */
    private int counter;
    /**
     * Modes controller.
     */
    private final GameModesController modesController;
    /**
     * Selected structure.
     */
    private IStructure selectedStructure;
    /**
     * Selected locomotive.
     */
    private Locomotive selectedLocomotive;
    /**
     * Selected wagon.
     */
    private IWagon selectedWagon;
    
    /**
     * Constructor.
     *
     * @param rectangle
     *            shape of this component.
     * @param newModesController
     *            modes controller.
     */
    public NormalMode(final Rectangle rectangle, final GameModesController newModesController) {
        super(rectangle);
        this.modesController = newModesController;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int posX, final int posY, final int clickCount) {
        if(button == SystemConstants.MOUSE_BUTTON_LEFT){
            final CoordinatesManager coordinatesManager = CoordinatesManager.getInstance();
            if (coordinatesManager.areCoordinatesOnScreen(new Vector2f(posX, posY))) {
                final Vector2f pos = coordinatesManager.getPositionByCoordinates(posX, posY);
                final ICell cell = Game.getInstance().getWorld().getTerrain().getCell((int) pos.getX(), (int) pos.getY());
                
                if (cell.getStructure() != null && cell.getStructure().getStructureType() != StructureTypes.FOREST) {
                    this.toggleSelectedStructure(cell.getStructure());
                    if (this.selectedStructure != null && this.selectedStructure.asProductionSite() != null) {
                        this.modesController.displayInfo((IProductionSite) this.selectedStructure);
                    } else if (this.selectedStructure != null && this.selectedStructure.asStation() != null) {
                        this.modesController.displayInfo((Station) this.selectedStructure);
                    }
                }
                if (cell.getStructure() == null || cell.getStructure().getStructureType() == StructureTypes.FOREST) {
                    this.toggleSelectedStructure(null);
                }
                else {
                    this.selectTrainComponent((int) pos.getX(), (int) pos.getY());
                    if (this.selectedLocomotive != null) {
                        this.modesController.displayInfo(this.selectedLocomotive);
                    } else if (this.selectedWagon != null) {
                        this.modesController.displayInfo(this.selectedWagon);
                    }
                }
            }
        }
    }
    
    /**
     * Method which takes care of finding which of the train components have
     * been selected.
     *
     * @param posX
     *            mouse click position X.
     * @param posY
     *            mouse click position Y.
     */
    public void selectTrainComponent(final int posX, final int posY) {
        final Collection<Train> trains = Game.getInstance().getWorld().getTrainManager().getTrains();
        for (final Train train : trains) {
            final Locomotive locomotive = train.getLocomotive();
            final List<IWagon> wagons = train.getWagons();
            
            final Track currentTrack = train.getCurrentTrack();
            if (currentTrack.getX() == posX && currentTrack.getY() == posY) {
                this.toggleSelectedLocomotive(locomotive);
            }
            if (train.getPreviousTracks().size() > 0 && wagons.size() > 0) {
                for (int counter = 0; counter < train.getPreviousTracks().size() - 1; counter++) {
                    final Track previousTrack = train.getPreviousTracks().get(counter);
                    if (posX == previousTrack.getX() && posY == previousTrack.getY() && wagons.get(counter) != null) {
                        this.toggleSelectedWagon(wagons.get(counter));
                    }
                }
            }
        }
    }
    
    /**
     * Deselects all selected items if they have or have not been selected.
     */
    public void removeAllSelected() {
        this.selectedWagon = null;
        this.selectedLocomotive = null;
        this.selectedStructure = null;
    }
    
    /**
     * Toggles selected wagon. Deselects if the wagon is the same or
     * if the mouse has been clicked on a cell which does not contain anything to select.
     *
     * @param wagon
     *            wagon selected with mouse from the terrain.
     */
    public void toggleSelectedWagon(final IWagon wagon) {
        if (wagon == null || this.selectedWagon == wagon) {
            this.selectedWagon = null;
            this.modesController.removeInfo();
        } else {
            this.removeAllSelected();
            this.selectedWagon = wagon;
        }
    }
    
    /**
     * Toggles selected locomotive. Deselects if the locomotive is the same or
     * if the mouse has been clicked on a cell which does not contain anything to select.
     *
     * @param locomotive
     *            locomotive selected with mouse from the terrain.
     */
    public void toggleSelectedLocomotive(final Locomotive locomotive) {
        if (locomotive == null || this.selectedLocomotive == locomotive) {
            this.selectedLocomotive = null;
            this.modesController.removeInfo();
        } else {
            this.removeAllSelected();
            this.selectedLocomotive = locomotive;
        }
    }
    
    /**
     * Toggles selected structure. Deselects if the structure is the same or
     * if the mouse has been clicked on a cell which does not contain anything to select.
     *
     * @param structure
     *            production site or station which is selected with mouse from the terrain.
     */
    public void toggleSelectedStructure(final IStructure structure) {
        if (structure == null || this.selectedStructure == structure) {
            this.selectedStructure = null;
            this.modesController.removeInfo();
        } else {
            this.removeAllSelected();
            this.selectedStructure = structure;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int posX, final int posY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int posX, final int posY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        this.updateCurrentCell(newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        this.updateCurrentCell(newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        // Implement if needed.
    }
    
    /**
     * Update current cell.
     *
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     */
    private void updateCurrentCell(final int posX, final int posY) {
        final CoordinatesManager coordinatesManager = CoordinatesManager.getInstance();
        if (coordinatesManager.areCoordinatesOnScreen(new Vector2f(posX, posY))) {
            final Vector2f pos = coordinatesManager.getPositionByCoordinates(posX, posY);
            this.hoverCell = Game.getInstance().getWorld().getTerrain().getCell((int) pos.getX(), (int) pos.getY());
        }
        
        if (this.getShape().contains(posX, posY)) {
            this.counter = 0;
        } else {
            this.hoverCell = null;
        }
    }
    
    /**
     * Update the counter of the box.
     *
     * @param input
     *            the slick input object reference.
     */
    private void updateCounter(final Input input) {
        if (input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_DOWN)
            || input.isKeyDown(Input.KEY_LEFT)
            || input.isKeyDown(Input.KEY_RIGHT)) {
            this.counter = 0;
            this.updateCurrentCell(input.getMouseX(), input.getMouseY());
        }
        
        ++this.counter;
    }
    
    /**
     * Render the mode on screen.
     *
     * @param graphics
     *            The graphics on which to draw.
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        this.updateCounter(container.getInput());
        if (this.counter > NormalMode.BOX_FRAMES_WAITING_TIME && this.hoverCell != SystemConstants.NULL
            && !Time.getInstance().isPaused()) {
            
            final int posX = container.getInput().getMouseX();
            final int posY = container.getInput().getMouseY();
            
            final boolean hasProductionSite = this.hoverCell.containsStructure() && null != this.hoverCell.getStructure()
                .asProductionSite();
            final int lineNo;
            if (hasProductionSite) {
                lineNo = 5;
            } else {
                lineNo = 3;
            }
            
            final String[] lines = new String[lineNo];
            int index = 0;
            
            lines[index] = this.hoverCell.getHoverText();
            ++index;
            
            if (hasProductionSite) {
                lines[index] = "Producing " + this.hoverCell.getStructure().asProductionSite().getWare().getTypeName();
                ++index;
                
                lines[index] = "Owner: " + this.hoverCell.getStructure().asProductionSite().getOwnersName();
                ++index;
            }
            
            switch (this.hoverCell.getCellType()) {
                case SEA:
                    lines[index] = "Depth: " + (9 - this.hoverCell.getHeight());
                    break;
                case MOUNTAIN:
                    lines[index] = "Height: " + this.hoverCell.asMountainCell().getRealHeight();
                    break;
                case GROUND:
                case RIVER:
                    lines[index] = "Height: " + this.hoverCell.getHeight();
                    break;
                default:
                    throw new RuntimeException("Code unreachable reached!");
            }
            
            ++index;
            
            if (this.hoverCell.canBuildTrack()) {
                lines[index] = "Track cost: " + this.hoverCell.getTrackCreationCost();
            } else {
                lines[index] = "Cannot build tracks here";
            }
            
            // Draw the nice cool box. Transform this stuff into a method if we need it elsewhere.
            final Font font = graphics.getFont();
            
            int stringwidth = 0;
            for (int i = 0; i < lines.length; ++i) {
                if (font.getWidth(lines[i]) > stringwidth) {
                    stringwidth = font.getWidth(lines[i]);
                }
            }
            final int lineHeight = font.getLineHeight();
            final int width = stringwidth + 2 * NormalMode.BOX_PADDING;
            final int height = lineNo * font.getLineHeight() + 2 * NormalMode.BOX_PADDING;
            
            graphics.setColor(NormalMode.BOX_COLOR);
            graphics.fillRoundRect(posX, posY, width, height, NormalMode.BOX_CORNER_RADIUS);
            graphics.drawRoundRect(posX, posY, width - 1, height - 1, NormalMode.BOX_CORNER_RADIUS);
            
            index = 0;
            FontUtils.drawCenter(font,
                lines[index],
                posX + NormalMode.BOX_PADDING,
                posY + NormalMode.BOX_PADDING
                + index
                * lineHeight,
                stringwidth);
            
            ++index;
            if (hasProductionSite) {
                FontUtils.drawCenter(font, lines[index], posX + NormalMode.BOX_PADDING, posY + NormalMode.BOX_PADDING
                    + index
                    * lineHeight, stringwidth);
                ++index;
                FontUtils.drawCenter(font, lines[index], posX + NormalMode.BOX_PADDING, posY + NormalMode.BOX_PADDING
                    + index
                    * lineHeight, stringwidth);
                ++index;
                
            }
            
            FontUtils.drawCenter(font, lines[index], posX + NormalMode.BOX_PADDING, posY + NormalMode.BOX_PADDING
                + index
                * lineHeight, stringwidth);
            ++index;
            
            FontUtils.drawCenter(font, lines[index], posX + NormalMode.BOX_PADDING, posY + NormalMode.BOX_PADDING
                + index
                * lineHeight,
                stringwidth);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        this.counter = 0;
        this.updateCurrentCell(coordX, coordY);
    }
}
