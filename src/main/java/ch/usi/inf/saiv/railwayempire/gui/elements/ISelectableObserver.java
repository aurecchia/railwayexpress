package ch.usi.inf.saiv.railwayempire.gui.elements;

/**
 * Observer for object containing selectable components.
 */
public interface ISelectableObserver<T> {
    /**
     * Called when the object gets selected.
     */
    void notifySelected(T selected);
    
    /**
     * Called when the object gets unselected.
     */
    void notifyUnSelected();
    
}
