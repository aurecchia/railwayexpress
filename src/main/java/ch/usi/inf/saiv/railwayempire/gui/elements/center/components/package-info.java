/**
 * Represent all of the Graphical User Interface components used in the central part of the application window.
 */
package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;