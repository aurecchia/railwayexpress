package ch.usi.inf.saiv.railwayempire.gui.modes;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Player;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.datastructures.Graph;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.model.structures.Track;
import ch.usi.inf.saiv.railwayempire.utilities.Coordinate;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Mode to handle user input and visualization of track creation.
 *
 * There are two ways to do this: keyboard input and drag n drop.
 * Because of this duality, this class is a mess.
 */
public final class TrackCreationMode extends AbstractGuiComponent implements IMode {

    /**
     * Next cell in the track creation process.
     */
    private ICell nextCell;
    /**
     * know if next cell is valid.
     */
    private boolean isNextCellValid;
    /**
     * Cost of building in the next track.
     */
    private int nextTrackCost;
    /**
     * Save terrain to avoid computing it every time.
     */
    private final Terrain terrain;

    /**
     * coordinates of the Track we are building FROM.
     */
    private int currentX;
    /**
     * coordinates of the Track we are building FROM.
     */
    private int currentY;
    /**
     * coordinates of the Track we are building TO.
     */
    private int nextX;
    /**
     * coordinates of the Track we are building TO.
     */
    private int nextY;

    /**
     * X coordinate of the cell where drag n drop started.
     */
    private int dragStartX;
    /**
     * Y coordinate of the cell where drag n drop started.
     */
    private int dragStartY;
    /*
     * X coordinate of the cell where drag n drop will end.
     */
    private int dragEndX;
    /**
     * Y coordinate of the cell where drag n drop will end.
     */
    private int dragEndY;

    /**
     * True when user is dragging. Used for rendering only when needed.
     */
    private boolean dragging;
    /**
     * List of coordinates representing the cells of the future path.
     */
    private List<Coordinate> dragNDropPath;
    /**
     * Cost of the path that is about to be created.
     */
    private int dragNDropPathCost;

    /**
     * Save resourceLoader as a field to prevent invoking ResourcesLoader.getInstance() every time.
     */
    private final ResourcesLoader resourcesLoader;

    /**
     * Default empty constructor.
     *
     * @param rectangle
     *            The perimeter on which to draw and respond.
     */
    public TrackCreationMode(final Rectangle rectangle) {
        super(rectangle);
        this.resourcesLoader = ResourcesLoader.getInstance();
        this.terrain = Game.getInstance().getWorld().getTerrain();
        this.currentX = -1;
        this.currentY = -1;
        this.nextX = -1;
        this.nextY = -1;
    }

    /**
     * Check if next cell is reachable, set it to null if its not.
     */
    private void checkNextCell() {
        if (this.terrain.areCoordinatesInRange(this.nextX, this.nextY)) {
            this.nextCell = this.terrain.getCell(this.nextX, this.nextY);
            this.isNextCellValid = this.canBuildOnWater() && this.nextCell.canBuildTrack()
                || (this.nextCell.containsStructure()
                    && this.nextCell.getStructure()
                    .getStructureType()
                    .equals(StructureTypes.TRACK));
            this.nextTrackCost = this.nextCell.getTrackCreationCost();
        } else {
            this.isNextCellValid = false;
        }
    }

    /**
     * Check if we can build on water.
     *
     * @return true if good.
     */
    private boolean canBuildOnWater() {
        switch (this.nextCell.getCellType()) {
            case GROUND:
            case MOUNTAIN:
                return true;
            case SEA:
            case RIVER:
                break;
            default:
                throw new RuntimeException("Code unreachable!");
        }

        final int incX = this.nextX - this.currentX;
        final int incY = this.nextY - this.currentY;

        for (int i=0; ; ++i) {

            final int posX = this.nextX + i * incX;
            final int posY = this.nextY + i * incY;

            if (!this.terrain.areCoordinatesInRange(posX, posY)) {
                return false;
            }

            final ICell cell = this.terrain.getCell(this.nextX + i * incX, this.nextY + i * incY);
            switch (cell.getCellType()) {
                case GROUND:
                case MOUNTAIN:
                    // we finally reached the coast, lets decide if we can build here.
                    if (cell.canBuildTrack() || cell.containsStructure()
                        && cell.getStructure().getStructureType() == StructureTypes.TRACK) {
                        // ground is good.
                        return true;
                    } else {
                        // ground is bad.
                        return false;
                    }
                case SEA:
                case RIVER:
                    /*
                     * Hey ho and a bottle of grog!
                     * We are in the sea.
                     * Is it too deep?
                     * Is there a bridge ahead?
                     * What do you see, scout?
                     * Can we build here?
                     */
                    if (!cell.canBuildTrack()) {
                        // too deep or some track is already here, i'm sorry.
                        return false;
                    }
                    break;

                default:
                    throw new RuntimeException("Code unreachable reached, uncool");
            }
        }
    }

    /**
     * create track.
     */
    public void create() {
        // checks.
        if (!Game.getInstance().getPlayer().canAfford(this.nextCell.getTrackCreationCost())) {
            Log.screen("You don't have enough money to do that!");
            return;
        }
        if (this.currentX == -1) {
            Log.screen("Choose a starting cell first!");
            return;
        }
        if (this.nextCell == SystemConstants.NULL || this.currentX == this.nextX && this.currentY == this.nextY) {
            Log.screen("You have to choose a direction first!");
            return;
        }
        if (!this.isNextCellValid) {
            Log.screen("You cannot build here!");
            return;
        }

        // subtract money.
        Game.getInstance().getPlayer().subtractMoney(this.nextCell.getTrackCreationCost());

        // remove forest.
        if (this.nextCell.containsStructure() && this.nextCell.getStructure().isRemovable()) {
            this.nextCell.removeStructure();
        }

        // build new track.
        final Track currentTrack = this.terrain.getCell(this.currentX, this.currentY).getStructure().asTrack();
        final Track nextTrack;
        if (this.nextCell.containsStructure()) {
            // next line should not return null since we already checked if its track in the method checkNextCell.
            nextTrack = this.nextCell.getStructure().asTrack();
            // but let's check anyway.
            if (null == nextTrack) {
                throw new NullPointerException();
            }
        } else {
            nextTrack = new Track(this.nextX, this.nextY);
            this.nextCell.setStructure(nextTrack);
        }

        currentTrack.addNeighbor(nextTrack);
        nextTrack.addNeighbor(currentTrack);

        for (final ICell cell : this.terrain.getCloseNeighborCells(this.nextX, this.nextY)) {
            if (cell.containsStructure()) {
                final Station station = cell.getStructure().asStation();
                if (station != SystemConstants.NULL) {
                    station.attachTracks();
                }
            }
        }

        final int tempX = this.currentX - this.nextX;
        final int tempY = this.currentY - this.nextY;
        this.currentX = this.nextX;
        this.currentY = this.nextY;
        this.nextX = this.currentX - tempX;
        this.nextY = this.currentY - tempY;
        this.checkNextCell();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        switch (key) {
            case Input.KEY_A:
                this.moveLeft();
                break;
            case Input.KEY_S:
                this.moveDown();
                break;
            case Input.KEY_D:
                this.moveRight();
                break;
            case Input.KEY_W:
                this.moveUp();
                break;
            case Input.KEY_SPACE:
                this.create();
                break;
            default:
                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int posX, final int posY, final int clickCount) {
        final Vector2f coordinates = CoordinatesManager.getInstance().getPositionByCoordinates(posX, posY);
        final int coordX = (int) coordinates.getX();
        final int coordY = (int) coordinates.getY();
        if (this.terrain.areCoordinatesInRange(coordX, coordY)) {
            final ICell cell = this.terrain.getCell(coordX, coordY);
            if (cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.TRACK) {
                this.nextCell = cell;
                this.currentX = coordX;
                this.currentY = coordY;
                this.nextX = coordX;
                this.nextY = coordY;
                this.checkNextCell();
            } else {
                Log.out("You have to start building on existing tracks!");
            }
        } else {
            Log.out("Cannot build track outside the world!");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        if (this.dragging) {
            final Vector2f coordinates = CoordinatesManager.getInstance().getPositionByCoordinates(newX, newY);
            this.updateDragAndDropPath((int) coordinates.getX(), (int) coordinates.getY());
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        // implement if needed.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int posX, final int posY) {
        if (button == SystemConstants.MOUSE_BUTTON_LEFT) {
            final Vector2f coordinates = CoordinatesManager.getInstance().getPositionByCoordinates(posX, posY);
            this.dragStartX = (int) coordinates.getX();
            this.dragStartY = (int) coordinates.getY();
            this.dragEndX = this.dragStartX;
            this.dragEndY = this.dragStartY;
            this.dragNDropPath = new ArrayList<Coordinate>();
            this.dragNDropPath.add(new Coordinate(this.dragStartX, this.dragStartY));
            this.dragNDropPathCost = this.terrain.getCell(this.dragStartX, this.dragStartY).getTrackCreationCost();
            this.dragging = true;
        } else {
            this.dragging = false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int posX, final int posY) {
        if (this.dragging) {
            final Vector2f coordinates = CoordinatesManager.getInstance().getPositionByCoordinates(posX, posY);
            this.updateDragAndDropPath((int) coordinates.getX(), (int) coordinates.getY());
            if (this.dragStartX != this.dragEndX || this.dragStartY != this.dragEndY) {
                this.buildDragAndDropTrack();
            }
            this.dragging = false;
        }
    }

    /**
     * Update both the dragEndX and dragEndY fields and the dragNDropPath list.
     *
     * @param newX
     *            x coordinate (grid coordinates).
     * @param newY
     *            y coordinate (grid coordinates).
     */
    private void updateDragAndDropPath(final int newX, final int newY) {
        if (newX == this.dragEndX && newY == this.dragEndY) {
            return; // Nothing to do here.
        }

        this.dragNDropPathCost = 0;

        this.dragEndX = newX;
        this.dragEndY = newY;

        int curX = newX;
        int curY = newY;

        this.dragNDropPath.clear();

        while (curX != this.dragStartX || curY != this.dragStartY) {

            this.dragNDropPath.add(new Coordinate(curX, curY));
            this.dragNDropPathCost += this.terrain.getCell(curX, curY).getTrackCreationCost();

            // update curX or curY:
            if (Math.abs(this.dragStartX - curX) > Math.abs(this.dragStartY - curY)) {
                if (curX > this.dragStartX) {
                    --curX;
                } else {
                    ++curX;
                }
            } else {
                if (curY > this.dragStartY) {
                    --curY;
                } else {
                    ++curY;
                }
            }
        }
        this.dragNDropPathCost += this.terrain.getCell(this.dragStartX, this.dragStartY).getTrackCreationCost();
        this.dragNDropPath.add(new Coordinate(this.dragStartX, this.dragStartY));
    }

    /**
     * build the drag n drop tracks.
     */
    private void buildDragAndDropTrack() {
        long cost = 0;
        final List<ICell> cells = new ArrayList<ICell>();
        final Player player = Game.getInstance().getPlayer();
        for (final Coordinate coordinate : this.dragNDropPath) {
            final ICell cell = this.terrain.getCell(coordinate.getX(), coordinate.getY());
            if (!this.canBuildDragNDropTracks(cell)) {
                Log.screen("Cannot build tracks here!");
                return;
            }
            if (!(cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.TRACK)) {
                cost += cell.getTrackCreationCost();
            }

            if (!player.canAfford(cost)) {
                Log.screen("Cannot afford!");
                return;
            }
            cells.add(cell);
        }

        Track previousTrack = null;
        for (int i = 0; i < this.dragNDropPath.size(); ++i) {
            final Coordinate coordinate = this.dragNDropPath.get(i);
            final ICell cell = cells.get(i);

            // Remove forest.
            if (cell.containsStructure() && cell.getStructure().isRemovable()) {
                cell.removeStructure();
            }
            // get track (get structure or create).
            final Track track;
            if (cell.containsStructure()) {
                track = cell.getStructure().asTrack();
            } else {
                track = new Track(coordinate.getX(), coordinate.getY());
                cell.setStructure(track);

            }

            // attach previous neighbor.
            if (null != previousTrack) {
                previousTrack.addNeighbor(track);
                track.addNeighbor(previousTrack);
            }
            previousTrack = track;

            // attach stations
            final List<ICell> neighborCells = this.terrain.getCloseNeighborCells(coordinate.getX(), coordinate.getY());
            for (final ICell neighbor : neighborCells) {
                if (neighbor.containsStructure()) {
                    final Station station = neighbor.getStructure().asStation();
                    if (null != station) {
                        station.attachTracks();
                        break;
                    }
                }
            }
        }

        player.subtractMoney(cost);
    }

    /**
     * User input: move down.
     */
    public void moveDown() {
        if (this.nextX == this.currentX && this.nextY == this.currentY + 1) {
            this.create();
        } else if (this.currentX != -1 && this.isOnGround()) {
            this.nextX = this.currentX;
            this.nextY = this.currentY + 1;
            this.checkNextCell();
        }
    }

    /**
     * User input: move left.
     */
    public void moveLeft() {
        if (this.nextX == this.currentX - 1 && this.nextY == this.currentY) {
            this.create();
        } else if (this.currentX != -1 && this.isOnGround()) {
            this.nextX = this.currentX - 1;
            this.nextY = this.currentY;
            this.checkNextCell();
        }
    }

    /**
     * User input: move right.
     */
    public void moveRight() {
        if (this.nextX == this.currentX + 1 && this.nextY == this.currentY) {
            this.create();
        } else if (this.currentX != -1 && this.isOnGround()) {
            this.nextX = this.currentX + 1;
            this.nextY = this.currentY;
            this.checkNextCell();
        }
    }

    /**
     * User input: move up.
     */
    public void moveUp() {
        if (this.nextX == this.currentX && this.nextY == this.currentY - 1) {
            this.create();
        } else if (this.currentX != -1 && this.isOnGround()) {
            this.nextX = this.currentX;
            this.nextY = this.currentY - 1;
            this.checkNextCell();
        }
    }

    /**
     * Check if we are currently on ground or on water.
     *
     * @return true if we are on the ground.
     */
    private boolean isOnGround() {
        switch (this.terrain.getCell(this.currentX, this.currentY).getCellType()) {
            case GROUND:
            case MOUNTAIN:
                return true;
            case SEA:
            case RIVER:
                return false;
            default:
                throw new RuntimeException("Code unreachable");
        }
    }

    /**
     * Undo the last operation.
     */
    public void destroy() {
        // get the current track.
        if (this.currentX >= 0) {
            final ICell currentCell = this.terrain.getCell(this.currentX, this.currentY);
            if (currentCell.containsStructure()) {
                final Track currentTrack = currentCell.getStructure().asTrack();
                if (null == currentTrack) {
                    throw new NullPointerException("BAD");
                }
                final Graph<Track> trackGraph = currentTrack.getGraph();
                if (trackGraph.size() <= 1) {
                    Log.screen("Cannot delete the last track!");
                } else {
                    final List<Track> neighborsList = currentTrack.getNeighbors();
                    trackGraph.removeNode(currentTrack);
                    currentCell.removeStructure();
                    for (final Track track : neighborsList) {
                        track.updateTexture();
                    }
                    if (!neighborsList.isEmpty()) {
                        final Track neighbor = neighborsList.get(0);
                        this.nextX = this.currentX;
                        this.nextY = this.currentY;
                        this.currentX = neighbor.getX();
                        this.currentY = neighbor.getY();
                        this.checkNextCell();
                    } else {
                        this.currentX = this.currentY = -1;
                        this.checkNextCell();
                    }
                }
            } else {
                Log.screen("Current Cell does not contain tracks!");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {

        final CoordinatesManager coordinatesManager = CoordinatesManager.getInstance();

        if (this.currentX != -1) {

            final String image = this.isNextCellValid ? "BUILDING_TILE" : "HIGHLIGHT_NEGATIVE";

            final Vector2f coords = coordinatesManager.getTransform().transform(new Vector2f(this.nextX, this.nextY));

            this.resourcesLoader.getImage(image).draw(coords.getX(),
                coords.getY() - coordinatesManager.getHeightOffset(this.nextCell),
                coordinatesManager.getScale());

            if (this.isNextCellValid) {

                GraphicsUtils.drawCostBox(graphics,
                    this.nextTrackCost,
                    (int) coords.getX() + 50,
                    (int) coords.getY() + 50);

                this.renderTracks();
            }
        }

        if (this.dragging) {
            final Image validImage = this.resourcesLoader.getImage("BUILDING_TILE");
            final Image invalidImage = this.resourcesLoader.getImage("HIGHLIGHT_NEGATIVE");
            for (final Coordinate coordinate : this.dragNDropPath) {
                final Vector2f coords = coordinatesManager.getCoordinatesByPosition(coordinate.getX(),
                    coordinate.getY());
                final ICell cell = this.terrain.getCell(coordinate.getX(), coordinate.getY());
                if (this.canBuildDragNDropTracks(cell)) {
                    validImage.draw(coords.getX(),
                        coords.getY() - coordinatesManager.getHeightOffset(cell),
                        coordinatesManager.getScale());
                } else {
                    invalidImage.draw(coords.getX(),
                        coords.getY() - coordinatesManager.getHeightOffset(cell),
                        coordinatesManager.getScale());
                }
            }
            GraphicsUtils.drawCostBox(graphics,
                this.dragNDropPathCost,
                container.getInput().getMouseX() + 50,
                container.getInput().getMouseY() + 50);
        }
    }

    /**
     * Check if we can build drag n drop tracks.
     *
     * @param cell
     *            the cell we will build on.
     * @return true if it's all good.
     */
    private boolean canBuildDragNDropTracks(final ICell cell) {
        switch (cell.getCellType()) {
            case SEA:
            case RIVER:
                return false;
            case GROUND:
            case MOUNTAIN:
                break;
            default:
                throw new RuntimeException("Unreachable Code Reached!");
        }

        if (cell.canBuildTrack()) {
            return true;
        }

        return cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.TRACK;
    }

    /**
     * Render the ghost tracks.
     */
    private void renderTracks() {
        // draw structure.
        final ICell cell = this.terrain.getCell(this.nextX, this.nextY);

        String textureString = "TRACK_";

        switch (cell.getCellType()) {
            case GROUND:
            case MOUNTAIN:
                textureString += "GROUND_GHOST_";
                break;
            case SEA:
                textureString += "SEA_GHOST_";
                break;
            case RIVER:
                textureString += "RIVER_GHOST_";
                break;
            default:
                throw new RuntimeException("Unreachable Code reached!");
        }

        if (this.nextX == this.currentX) {
            textureString += "1";
        } else {
            textureString += "2";
        }
        final Image texture = this.resourcesLoader.getImage(textureString);

        final CoordinatesManager coordinatesManager = CoordinatesManager.getInstance();
        final Vector2f coords = coordinatesManager.getCoordinatesByPosition(this.nextX, this.nextY);

        if (cell.getCellType() == CellTypes.SEA) {
            final float vOffset = texture.getHeight() - 120;
            texture.draw(coords.getX(), coords.getY() - coordinatesManager.getHeightOffset(cell)
                - vOffset
                * coordinatesManager.getScale(), coordinatesManager.getScale());

        } else {
            texture.draw(coords.getX(),
                coords.getY() - coordinatesManager.getHeightOffset(cell),
                coordinatesManager.getScale());
        }
    }

    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
    }
}
