package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.controllers.CenterPanelsController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.IGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Button panel.
 */
public class StatesButtonsPanel extends AbstractPanel {
    
    /**
     * The margin between buttons.
     */
    private static final int BUTTONS_MARGIN = 5;
    /**
     * The width of a small button.
     */
    private static final int BUTTON_WIDTH = ResourcesLoader.getInstance().getImage("NORMAL_MODE_BUTTON").getWidth();
    /**
     * The height of a small button.
     */
    private static final int BUTTON_HEIGHT = ResourcesLoader.getInstance().getImage("NORMAL_MODE_BUTTON").getHeight();
    
    /**
     * Constructor.
     * 
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @param centerPanelsController
     *            center panels controller.
     */
    public StatesButtonsPanel(final int posX, final int posY, final CenterPanelsController centerPanelsController) {
        super(new Rectangle(posX,
            posY,
            StatesButtonsPanel.BUTTON_WIDTH * 4 + StatesButtonsPanel.BUTTONS_MARGIN * 5,
            StatesButtonsPanel.BUTTON_HEIGHT + StatesButtonsPanel.BUTTONS_MARGIN));
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        
        // World button.
        final Button worldButton = new Button(
            loader.getImage("NORMAL_MODE_BUTTON"),
            loader.getImage("NORMAL_MODE_BUTTON_HOVER"),
            loader.getImage("NORMAL_MODE_BUTTON_PRESSED"),
            this.getHorizontalPosition(1), this.getY() + StatesButtonsPanel.BUTTONS_MARGIN);
        worldButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                StatesButtonsPanel.this.releaseAllButtons();
                worldButton.holdButton();
                centerPanelsController.enterWorldPanel();
            }
        });
        
        // Train Components button.
        final Button trainComponentsButton = new Button(
            loader.getImage("TRAIN_MANAGER_BUTTON"),
            loader.getImage("TRAIN_MANAGER_BUTTON_HOVER"),
            loader.getImage("TRAIN_MANAGER_BUTTON_PRESSED"),
            this.getHorizontalPosition(3), this.getY() + StatesButtonsPanel.BUTTONS_MARGIN);
        trainComponentsButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                StatesButtonsPanel.this.releaseAllButtons();
                trainComponentsButton.holdButton();
                centerPanelsController.enterTrainEditorPanel();
            }
        });
        
        final Button trainBuyComponentButton = new Button(
            loader.getImage("COMPONENTS_STORE_BUTTON"),
            loader.getImage("COMPONENTS_STORE_BUTTON_HOVER"),
            loader.getImage("COMPONENTS_STORE_BUTTON_PRESSED"),
            this.getHorizontalPosition(2), this.getY() + StatesButtonsPanel.BUTTONS_MARGIN);
        trainBuyComponentButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                StatesButtonsPanel.this.releaseAllButtons();
                trainBuyComponentButton.holdButton();
                centerPanelsController.enterTrainBuyComponentsPanel();
            }
        });
        
        // Train Route Button.
        final Button trainRouteButton = new Button(
            loader.getImage("ROUTE_CREATION_BUTTON"),
            loader.getImage("ROUTE_CREATION_BUTTON_HOVER"),
            loader.getImage("ROUTE_CREATION_BUTTON_PRESSED"),
            
            this.getHorizontalPosition(4), this.getY() + StatesButtonsPanel.BUTTONS_MARGIN);
        
        trainRouteButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                StatesButtonsPanel.this.releaseAllButtons();
                trainRouteButton.holdButton();
                centerPanelsController.enterTrainRoutePanel();
            }
        });
        
        this.add(worldButton);
        this.add(trainComponentsButton);
        this.add(trainBuyComponentButton);
        this.add(trainRouteButton);
        
        worldButton.holdButton();
        
        this.setBackground(loader.getImage("BACKGROUND"));
    }
    
    /**
     * Unhold all buttons.
     */
    private void releaseAllButtons() {
        for (final IGuiComponent button : this.getChildrenComponents()) {
            ((Button) button).unHoldButton();
        }
    }
    
    /**
     * Returns the horizontal position of the button given the button number.
     * 
     * @param buttonNumber
     *            The number of the button.
     * @return The horizontal offset.
     */
    private int getHorizontalPosition(final int buttonNumber) {
        return this.getX() + StatesButtonsPanel.BUTTONS_MARGIN
            * buttonNumber
            + StatesButtonsPanel.BUTTON_WIDTH
            * (buttonNumber - 1);
    }
}
