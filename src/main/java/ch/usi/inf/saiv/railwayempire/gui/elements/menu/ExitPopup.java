package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameplayController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Popup that is shown when the user presses exit in the pausemenu.
 */
public class ExitPopup extends SimplePanel {
    
    /**
     * Background color.
     */
    private static final Color BACKGROUNDCOLOR = new Color(57, 51, 46);
    
    /**
     * Distance between buttons.
     */
    private static final int BUTTON_DISTANCE = 10;
    
    /**
     * The text of the popup.
     */
    private final Label label;
    
    /**
     * Save button.
     */
    private final Button saveButton;
    
    /**
     * Create a popup to help the user exit if he doesn't like this awesome
     * game.
     * 
     * @param newShape
     *            Shape of the popup.
     * @param controller
     *            controller.
     * @param container
     *            container.
     * @param pauseMenuPanel
     *            the parent.
     */
    public ExitPopup(final Shape newShape, final GameplayController controller, final GameContainer container,
        final PauseMenuPanel pauseMenuPanel) {
        super(newShape);
        final Image saveImage = ResourcesLoader.getInstance().getImage("SAVE_BUTTON");
        final Image exitImage = ResourcesLoader.getInstance().getImage("EXIT_BUTTON");
        final Image cancelImage = ResourcesLoader.getInstance().getImage("CANCEL_BUTTON");
        
        
        this.saveButton = new Button(saveImage, ResourcesLoader.getInstance().getImage("SAVE_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("SAVE_BUTTON_PRESSED"), ResourcesLoader.getInstance().getImage(
                "SAVE_BUTTON_DISABLED"), this.getX() + this.getWidth() / 2
                - saveImage.getWidth() / 2, this.getY() + this.getHeight() / 2 + ExitPopup.BUTTON_DISTANCE);
        final Button exitButton = new Button(exitImage, ResourcesLoader.getInstance().getImage("EXIT_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("EXIT_BUTTON_PRESSED"), this.saveButton.getX()
            + this.saveButton.getWidth() + ExitPopup.BUTTON_DISTANCE, this.saveButton.getY());
        final Button cancelButton = new Button(cancelImage,
            ResourcesLoader.getInstance().getImage("CANCEL_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage(
                "CANCEL_BUTTON_PRESSED"), this.saveButton.getX() - ExitPopup.BUTTON_DISTANCE - cancelImage.getWidth(),
                this.saveButton.getY());
        
        this.saveButton.setListener(new MouseListener() {
            
            @Override
            public void actionPerformed(final Button button) {
                pauseMenuPanel.toggleExitPopup();
                controller.setSavePanel();
            }
        });
        
        exitButton.setListener(new MouseListener() {
            
            @Override
            public void actionPerformed(final Button button) {
                controller.exitGame();
            }
        });
        
        cancelButton.setListener(new MouseListener() {
            
            @Override
            public void actionPerformed(final Button button) {
                pauseMenuPanel.toggleExitPopup();
            }
        });
        
        this.add(this.saveButton);
        this.add(exitButton);
        this.add(cancelButton);
        
        this.label = new Label(this.getX(), this.getY() + this.getHeight() / 2
            - container.getGraphics().getFont().getLineHeight() - ExitPopup.BUTTON_DISTANCE, this.getWidth(),
            this.getHeight(),
            "Are you sure you want to exit?", Alignment.CENTER);
        this.add(this.label);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setColor(ExitPopup.BACKGROUNDCOLOR);
        graphics.fillRoundRect(this.getX(), this.getY(), this.getWidth(), this.getHeight(), 15);
        graphics.setColor(Color.black);
        graphics.drawRoundRect(this.getX(), this.getY(), this.getWidth() - 1, this.getHeight() - 1, 15);
        super.render(container, graphics);
    }
    
    /**
     * Get the label.
     * 
     * @return the label.
     */
    public Label getLabel() {
        return this.label;
    }
    
    /**
     * Get the save button.
     * 
     * @return the button.
     */
    public Button getSaveButton() {
        return this.saveButton;
    }
}
