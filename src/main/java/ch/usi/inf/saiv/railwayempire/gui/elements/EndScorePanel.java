package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.elements.menu.HighScorePanel;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * A panel that displays both the high scores and the background.
 */
public class EndScorePanel extends SimplePanel {
    
    /**
     * Constructor.
     * 
     * @param newShape
     *            shape.
     * @param container
     *            game container.
     */
    public EndScorePanel(final Shape newShape, final GameContainer container) {
        super(newShape);
        this.add(new HighScorePanel(newShape, container));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        ResourcesLoader.getInstance()
        .getImage("SCOREBOARD_BACKGROUND")
        .draw(0, 0, container.getWidth(), container.getHeight());
        super.render(container, graphics);
    }
}
