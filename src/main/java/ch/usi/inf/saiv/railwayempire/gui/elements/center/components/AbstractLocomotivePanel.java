package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Panel representing the details of a locomotive.
 * 
 */
public abstract class AbstractLocomotivePanel extends AbstractComponentPanel {
    
    /**
     * The locomotive represented by the panel.
     */
    private final Locomotive component;
    
    /**
     * Instantiate a new panel.
     * 
     * @param shape
     *            The shape of the panel.
     * @param locomotive
     *            The locomotive to represent.
     */
    public AbstractLocomotivePanel(final Shape shape, final Locomotive locomotive) {
        super(shape, ResourcesLoader.getInstance().getImage(locomotive.getTextureName() + "_E"));
        this.component = locomotive;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        
        //Details
        final float posX = this.getX() + 2
            * AbstractComponentPanel.getPadding()
            + AbstractComponentPanel.getIconWidth();
        float posY = this.getY() + AbstractComponentPanel.getPadding()
            + 2 * AbstractComponentPanel.getLineHeight();
        
        graphics.drawString("Speed " + this.component.getVelocityString(), posX, posY);
        posY += AbstractComponentPanel.getLineHeight();
        graphics.drawString("Hauls " + this.component.getWagonLimit() + " wagons", posX, posY);
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Locomotive getComponent() {
        return this.component;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public int getComponentAvailability() {
        return Game.getInstance().getPlayer().getWareHouse().getComponentAvailability(this.getComponent());
    }
    
}
