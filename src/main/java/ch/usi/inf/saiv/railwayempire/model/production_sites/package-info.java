/**
 * Provides the functionality to have structures which produce different wares for trains to trade with.
 * AbstractProduction site is created every time one of the sites is created, because abstract production site
 * provides basic functionality shared between all production sites.
 *
 * Building - produces people. It has 32 different textures. Should be placed in the city.
 *
 * CoalMine - produces coal. It has 2 different textures. Should be placed near the mountains.
 *
 * DrillRig - produces oil. Has 2 different animated textures, each has 5 frames. Should be placed near water.
 *
 * Farm - produces food. Has 6 different textures.
 *
 * FoodFactory - produces food. Has 3 different textures. Should be placed near the city.
 *
 * IronMine - produces iron. It has 2 different textures. Should be placed near the mountains.
 *
 * PostOffice - produces mail.
 *
 * RockCave - produces rock. It has 2 different textures. Should be placed near the mountains.
 *
 * SawMill - produces wood. It has 4 different textures. Should be placed near a forest.
 */
package ch.usi.inf.saiv.railwayempire.model.production_sites;