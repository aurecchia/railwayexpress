package ch.usi.inf.saiv.railwayempire.gui.states;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import ch.usi.inf.saiv.railwayempire.gui.GameState;
import ch.usi.inf.saiv.railwayempire.controllers.MainMenuController;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;



/**
 * Main menu state.
 */
public class MainMenuState extends BasicGameState {


    private SimplePanel currentPanel;

    private GameContainer gameContainer;


    /**
     * Constructor for the State.
     */
    public MainMenuState() {
    }

    /**
     * Returns the id of the state.
     *
     * @return The id of the state.
     */
    @Override
    public final int getID() {
        return GameState.MAIN_MENU.ordinal();
    }

    /**
     * Initializes the state.
     *
     * @param container
     *            The container of the game.
     * @param stateBasedGame
     *            The instance of the game.
     * @throws SlickException
     *             An exception.
     */
    @Override
    public final void init(final GameContainer container, final StateBasedGame stateBasedGame) throws SlickException {
        this.gameContainer = container;
        ResourcesLoader.getInstance().loadResources(false);
        new MainMenuController(this, container, stateBasedGame);
    }

    /**
     * Renders the state.
     *
     * @param container
     *         The container of the game.
     * @param stateBasedGame
     *         The instance of the game.
     * @param graphics
     *         The graphics to draw on.
     * @throws SlickException
     *         An exception.
     */
    @Override
    public final void render(final GameContainer container, final StateBasedGame stateBasedGame,
            final Graphics graphics) throws SlickException {
        graphics.setFont(FontUtils.FONT);
        final Image backGround = ResourcesLoader.getInstance().getImage("MAIN_MENU_BACKGROUND");
        backGround.draw(0, 0, container.getWidth(), container.getHeight());
        this.currentPanel.render(container, graphics);
    }

    /**
     * Updates the state of the game.
     *
     * @param container
     *         The container of the game.
     * @param stateBasedGame
     *         The instance of the game.
     * @param delta
     *         The time passed since the last update in milliseconds.
     * @throws SlickException
     *         An exception.
     */
    @Override
    public final void update(final GameContainer container, final StateBasedGame stateBasedGame,
            final int delta) throws SlickException {
        // Implement if needed.
    }

    /**
     * Automatically called when the mouse is clicked.
     *
     * @param button
     *            The button which was clicked.
     * @param coordX
     *            The x coordinate of the click.
     * @param coordY
     *            The y coordinate of the click.
     * @param clickCount
     *            The number of clicks.
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        this.currentPanel.mouseClicked(button, coordX, coordY, clickCount);
    }

    /**
     * Automatically called when the mouse is pressed.
     *
     * @param button
     *            The button which was clicked.
     * @param coordX
     *            The x coordinate of the click.
     * @param coordY
     *            The y coordinate of the click.
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        this.currentPanel.mousePressed(button, coordX, coordY);
    }

    /**
     * Automatically called when the mouse is released.
     *
     * @param button
     *            The button which was clicked.
     * @param coordX
     *            The x coordinate of the click.
     * @param coordY
     *            The y coordinate of the click.
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        this.currentPanel.mouseReleased(button, coordX, coordY);
    }

    /**
     * Automatically called when the mouse is dragged.
     *
     * @param oldX
     *            The x coordinate before the drag.
     * @param oldY
     *            The y coordinate before the drag.
     * @param newX
     *            The x coordinate after the drag.
     * @param newY
     *            The y coordinate after the drag.
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        this.currentPanel.mouseDragged(oldX, oldY, newX, newY);
    }

    /**
     * Automatically called when the mouse is dragged.
     *
     * @param oldX
     *            The x coordinate before the drag.
     * @param oldY
     *            The y coordinate before the drag.
     * @param newX
     *            The x coordinate after the drag.
     * @param newY
     *            The y coordinate after the drag.
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        this.currentPanel.mouseMoved(oldX, oldY, newX, newY);
    }

    /**
     * Automatically called when the mouse wheel is moved.
     *
     * @param charge
     *            The number of ticks the wheel has moved.
     */
    @Override
    public void mouseWheelMoved(final int charge) {
        final int coordX = this.gameContainer.getInput().getAbsoluteMouseX();
        final int coordY = this.gameContainer.getInput().getAbsoluteMouseY();
        this.currentPanel.mouseWheelMoved(charge, coordX, coordY);
    }

    /**
     * Automatically called when keys are pressed.
     *
     * @param key
     *            The integer corresponding to the key.
     * @param character
     *            The character corresponding to the key.
     */
    @Override
    public void keyPressed(final int key, final char character) {
    }


    /**
     * Sets the current panel.
     *
     * @param currentPanel
     *            The panel to set.
     */
    public void setCurrentPanel(final SimplePanel currentPanel) {
        this.currentPanel = currentPanel;
    }
}
