package ch.usi.inf.saiv.railwayempire.gui.elements;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Class representing a panel which holds some children.
 * 
 * Main responsibility is to hold a list of children gui components and to pass events to the children: execute render()
 * on children when render() is called, execute the mouse event methods on the right child, and so on.
 * 
 * Another responsibility is to hold a background image or color and render it before the children.
 */
public abstract class AbstractPanel extends AbstractGuiComponent {
    
    /**
     * List of children components.
     */
    private final List<IGuiComponent> childrenComponents;
    
    /**
     * Background Image of this component. NULL if not applicable.
     * 
     * If both an image and a color are set, the image is drawn on top.
     */
    private Image backgroundImage;
    
    /**
     * Background Color of this component. NULL if not applicable.
     * 
     * If both an image and a color are set, the image is drawn on top.
     */
    private Color backgroundColor;
    
    /**
     * Constructor.
     * 
     * @param newShape
     *            a rectangle representing the panel shape.
     */
    protected AbstractPanel(final Shape newShape) {
        super(newShape);
        this.childrenComponents = new ArrayList<IGuiComponent>();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        if (this.backgroundColor != SystemConstants.NULL) {
            final Color previousColor = graphics.getColor();
            graphics.setColor(this.backgroundColor);
            graphics.fill(this.getShape());
            graphics.setColor(previousColor);
        }
        
        if (this.backgroundImage != SystemConstants.NULL) {
            graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight(), this.backgroundImage, 0, 0);
        }
        
        for (final IGuiComponent component : this.childrenComponents) {
            component.render(container, graphics);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        for (final IGuiComponent component : this.childrenComponents) {
            if (component.contains(coordX, coordY)) {
                component.mouseClicked(button, coordX, coordY, clickCount);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        for (final IGuiComponent component : this.childrenComponents) {
            if (component.contains(coordX, coordY)) {
                component.mousePressed(button, coordX, coordY);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        for (final IGuiComponent component : this.childrenComponents) {
            if (component.contains(coordX, coordY)) {
                component.mouseReleased(button, coordX, coordY);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        for (final IGuiComponent component : this.childrenComponents) {
            if (component.contains(newX, newY) || component.contains(oldX, oldY)) {
                component.mouseDragged(oldX, oldY, newX, newY);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        for (final IGuiComponent component : this.childrenComponents) {
            if (component.contains(newX, newY) || component.contains(oldX, oldY)) {
                component.mouseMoved(oldX, oldY, newX, newY);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        for (final IGuiComponent component : this.childrenComponents) {
            if (component.contains(coordX, coordY)) {
                component.mouseWheelMoved(charge, coordX, coordY);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        for (final IGuiComponent component : this.childrenComponents) {
            component.keyPressed(key, character);
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void setLocation(final int newPosX, final int newPosY) {
        final int deltaX = this.getX() - newPosX;
        final int deltaY = this.getY() - newPosY;
        for (final IGuiComponent child : this.getChildrenComponents()) {
            child.setLocation(child.getX() - deltaX, child.getY() - deltaY);
        }
        super.setLocation(newPosX, newPosY);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void setX(final int newX) {
        final int delta = this.getX() - newX;
        for (final IGuiComponent child : this.getChildrenComponents()) {
            child.setX(child.getX() - delta);
        }
        super.setX(newX);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void setY(final int newY) {
        final int delta = this.getY() - newY;
        for (final IGuiComponent child : this.getChildrenComponents()) {
            child.setY(child.getY() - delta);
        }
        super.setY(newY);
    }
    
    /**
     * Set the background image.
     * 
     * @param background
     *            background image.
     */
    public final void setBackground(final Image background) {
        this.backgroundImage = background;
    }
    
    /**
     * Set the background color.
     * 
     * @param background
     *            background color.
     */
    public void setBackground(final Color background) {
        this.backgroundColor = background;
    }
    
    /**
     * Get the background color.
     * 
     * @return The color of the background or <code>null</code> if it is not defined.
     */
    public Color getBackgroundColor() {
        return this.backgroundColor;
    }
    
    /**
     * Get the background image.
     * 
     * @return The image of the background or <code>null</code> if it is not defined.
     */
    public Image getBackgroundImage() {
        return this.backgroundImage;
    }
    
    /**
     * Add a new children component.
     * 
     * @param component
     *            new children component.
     */
    public void add(final IGuiComponent component) {
        if (!this.childrenComponents.contains(component)) {
            this.childrenComponents.add(component);
        }
    }
    
    /**
     * Remove a children component.
     * 
     * @param component
     *            children component to remove.
     */
    public void remove(final IGuiComponent component) {
        this.childrenComponents.remove(component);
    }
    
    /**
     * Get the list of children components.
     * 
     * @return list of children components.
     */
    public final List<IGuiComponent> getChildrenComponents() {
        return this.childrenComponents;
    }
    
}
