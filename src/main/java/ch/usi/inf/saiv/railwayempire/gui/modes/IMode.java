package ch.usi.inf.saiv.railwayempire.gui.modes;

import ch.usi.inf.saiv.railwayempire.gui.viewers.IViewer;

/** interface for the modes. */
public interface IMode extends IViewer {
    
    /**
     * Handler for the event mouse clicked.
     *
     * @param button
     *         mouse button pressed.
     * @param posX
     *         position x.
     * @param posY
     *         position y.
     * @param clickCount
     *         number of clicks.
     */
    void mouseClicked(final int button, final int posX, final int posY, final int clickCount);
    
    /**
     * Handler for the event mouse pressed.
     *
     * @param button
     *         mouse button pressed.
     * @param posX
     *         position x.
     * @param posY
     *         position y.
     */
    void mousePressed(final int button, final int posX, final int posY);
    
    /**
     * Handler for the event mouse released.
     *
     * @param button
     *         mouse button pressed.
     * @param posX
     *         position x.
     * @param posY
     *         position y.
     */
    void mouseReleased(final int button, final int posX, final int posY);
    
    /**
     * Handler for the event mouse dragged.
     *
     * @param oldX
     *         the x position of the mouse before the movement.
     * @param oldY
     *         the y position of the mouse before the movement.
     * @param newX
     *         the x position of the mouse after the movement.
     * @param newY
     *         the y position of the mouse after the movement.
     */
    void mouseDragged(final int oldX, final int oldY, final int newX, final int newY);
    
    /**
     * Handler for the event mouse moved.
     *
     * @param oldX
     *         the x position of the mouse before the movement.
     * @param oldY
     *         the y position of the mouse before the movement.
     * @param newX
     *         the x position of the mouse after the movement.
     * @param newY
     *         the y position of the mouse after the movement.
     */
    void mouseMoved(final int oldX, final int oldY, final int newX, final int newY);
    
    /**
     * Handler for the event mouse wheel moved.
     * 
     * @param charge
     *            charge.
     * @param coordX
     *            x coordinate of mouse.
     * @param coordY
     *            y coordinate of mouse.
     */
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY);
    
    /**
     * Handler for the event key pressed.
     *
     * @param key
     *         int value of key pressed.
     * @param character
     *         character pressed.
     */
    void keyPressed(final int key, final char character);
}
