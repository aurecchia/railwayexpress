package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Bottom Panel for utility display according to selected wagon if selected at all.
 */
public final class WagonUtilityPanel extends AbstractWorldInfoUtilityPanel {
    
    /**
     * Label width.
     */
    private final static int LABEL_WIDTH = 180;
    /**
     * Label height.
     */
    private final static int LABEL_HEIGHT = 20;
    /**
     * The margin for the logo.
     */
    private final static int IMAGE_MARGIN = 8;
    /**
     * Margin.
     */
    private final static int MARGIN = 5;
    /**
     * The image of the logo.
     */
    private final Button wagonLogo;
    
    /**
     * Creates the panel for displaying selected wagon information.
     *
     * @param rectangle
     *            utility rectangle.
     * @param wagon
     *            selected wagon.
     * @param modesController
     *            modes controller.
     */
    public WagonUtilityPanel(final Rectangle rectangle, final IWagon wagon,
        final GameModesController modesController) {
        super(rectangle);
        
        Train train = null;
        for (final Train tempTrain : Game.getInstance().getWorld().getTrainManager().getTrains()) {
            for (final IWagon tempWagon : tempTrain.getWagons()) {
                if(tempWagon.equals(wagon)){
                    train = tempTrain;
                }
            }
        }
        final Train realTrain = train;
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        this.wagonLogo =
            new Button(ResourcesLoader.getInstance().getImage(wagon.getTextureName() + "_BUTTON"),
                loader.getImage(wagon.getTextureName() + "_BUTTON_HOVER"),
                loader.getImage(wagon.getTextureName() + "_BUTTON_PRESSED"),
                this.getX(),
                this.getY());
        this.wagonLogo.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                CoordinatesManager.getInstance().centerCameraOn(realTrain.getX(), realTrain.getY());
            }
        });
        
        this.add(this.wagonLogo);
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2
            * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY(),
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            wagon.getName()));
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2
            * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY() + WagonUtilityPanel.MARGIN + WagonUtilityPanel.LABEL_HEIGHT,
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            GraphicsUtils.formatCost(wagon.getCost())));
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2 * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY() + WagonUtilityPanel.MARGIN + WagonUtilityPanel.LABEL_HEIGHT * 2,
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            "Transporting: " + wagon.getWare().getTypeName()));
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2 * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY() + WagonUtilityPanel.MARGIN + WagonUtilityPanel.LABEL_HEIGHT * 3,
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String state = "State: " + realTrain.getState();
                GraphicsUtils.drawShadowText(graphics, state, this.getX(), this.getY());
            }
        });
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2
            * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY() + WagonUtilityPanel.MARGIN + WagonUtilityPanel.LABEL_HEIGHT * 4,
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            "Carry limit: " + wagon.getUnitLimit()));
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2
            * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY() + WagonUtilityPanel.MARGIN + WagonUtilityPanel.LABEL_HEIGHT * 5,
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String loaded = "Loaded amount: " + wagon.getLoadedAmount();
                GraphicsUtils.drawShadowText(graphics, loaded, this.getX(), this.getY());
            }
        });
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2
            * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY() + WagonUtilityPanel.MARGIN + WagonUtilityPanel.LABEL_HEIGHT * 6,
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String time = "Loading time: " + wagon.getLoadWaitingTime();
                GraphicsUtils.drawShadowText(graphics, time, this.getX(), this.getY());
            }
        });
        
        this.add(new Label(this.getX() + WagonUtilityPanel.MARGIN + this.wagonLogo.getWidth() + 2
            * WagonUtilityPanel.IMAGE_MARGIN,
            this.getY() + WagonUtilityPanel.MARGIN + WagonUtilityPanel.LABEL_HEIGHT * 7,
            WagonUtilityPanel.LABEL_WIDTH,
            WagonUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String nextStop = "Next stop: " + realTrain.getRoute().getCurrentStation().getName();
                GraphicsUtils.drawShadowText(graphics, nextStop, this.getX(), this.getY());
            }
        });
    }
}
