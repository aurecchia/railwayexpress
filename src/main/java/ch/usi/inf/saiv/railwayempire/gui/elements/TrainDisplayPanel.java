package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.Time;

/**
 * Display a side view of the train
 * 
 */
public class TrainDisplayPanel extends LayoutPanel {
    
    /**
     * Width/Height ratio of a component icon (256x70)
     */
    private static float ICON_RATIO = 3.65714f;
    
    /**
     * The train to display
     */
    private Train train;
    
    /**
     * The amount of time to wait during autoscroll when reaching one end or the other of the train composition
     * rendering.
     */
    private int wait;
    
    /**
     * The different state for the automatic scrolling.
     * 
     */
    private enum State {
        WAIT_RIGHT, WAIT_LEFT, RIGHT, LEFT, STOPPING, STOPPED
    }
    
    /**
     * The current state for automatic scrolling
     */
    private State state;
    
    /**
     * Instantiate a new panel
     * 
     * @param rectangle
     *            The rectangle in which to render the panel
     * @param train
     *            The train to display
     */
    public TrainDisplayPanel(final Rectangle rectangle, final Train train) {
        super(rectangle, LayoutPanel.HORIZONTAL, 1);
        this.wait = 0;
        this.state = State.STOPPED;
        this.setTrain(train);
        this.buildTrain();
        
    }
    
    /**
     * Set the train to display.
     * 
     * @param newTrain
     *            The new train to display.
     */
    public final void setTrain(final Train newTrain) {
        this.train = newTrain;
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        this.buildTrain();
        if (this.state != State.STOPPED) {
            this.autoScroll();
        }
        super.render(container, graphics);
    }
    
    /**
     * Build the representation of the train.
     */
    public void buildTrain() {
        this.getChildrenComponents().clear();
        if (this.train == null) {
            return;
        }
        
        this.add(new TrainComponentDisplayPanel(new Rectangle(0, 0,
            this.getHeight() * TrainDisplayPanel.ICON_RATIO, this.getHeight()), this.train.getLocomotive()));
        
        for (final IWagon wagon : this.train.getWagons()) {
            this.add(new TrainComponentDisplayPanel(new Rectangle(0, 0,
                this.getHeight() * TrainDisplayPanel.ICON_RATIO, this.getHeight()), wagon));
        }
    }
    
    /**
     * Enable scrolling of the train.
     */
    public void enableAutoScroll() {
        this.state = this.state == State.STOPPING ? State.LEFT : State.RIGHT;
    }
    
    /**
     * Disable scrolling of the train.
     */
    public void disableAutoScroll() {
        this.state = State.STOPPING;
    }
    
    /**
     * Scroll the train automatically.
     */
    public void autoScroll() {
        if (!this.isScrollable()) {
            return;
        }
        switch (this.state) {
            case LEFT:
                if (this.getOffset() == 0) {
                    this.wait = 0;
                    this.state = State.WAIT_LEFT;
                } else {
                    this.scroll(1);
                }
                break;
            case RIGHT:
                if (this.getOffset() == this.getWidth() - this.getAbsoluteLength()) {
                    this.wait = 0;
                    this.state = State.WAIT_RIGHT;
                } else {
                    this.scroll(-1);
                }
                break;
            case WAIT_LEFT:
                this.wait += Time.getInstance().getDelta();
                if (this.wait > 120000) {
                    this.state = State.RIGHT;
                }
                break;
            case WAIT_RIGHT:
                this.wait += Time.getInstance().getDelta();
                if (this.wait > 120000) {
                    this.state = State.LEFT;
                }
                break;
            case STOPPING:
                if (this.getOffset() == 0) {
                    this.wait = 0;
                    this.state = State.STOPPED;
                } else {
                    this.scroll(1);
                }
                break;
            default:
                break;
        }
        
    }
}
