package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.MainMenuController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.Pair;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Highscores;

/**
 * Panel displaying the high scores.
 * 
 */
public class HighScorePanel extends SimplePanel {
    /**
     * A transparent black.
     */
    private static final Color FADE_COLOR = new Color(0, 0, 0, 150);
    /**
     * Background color.
     */
    private static final Color BACKGROUND_COLOR = new Color(57, 51, 46);
    /**
     * Distance between buttons.
     */
    private static final int BUTTONDISTANCE = 10;
    
    /**
     * Background height.
     */
    private final int backgroundHeight;
    /**
     * Background width.
     */
    private final int backgroundWidth;
    /**
     * True if in a panel, false if in another one.
     */
    private final boolean endGame;

    /**
     * Instantiate a new high scores panel for the main menu.
     * 
     * @param newShape
     *            The shape of the panel.
     * @param controller
     *            The controller handling this panel.
     */
    public HighScorePanel(final Shape newShape, final MainMenuController controller) {
        super(newShape);
        this.backgroundHeight = 400;
        this.backgroundWidth = 500;
        final Image cancelImage = ResourcesLoader.getInstance().getImage("BACK_BUTTON");
        
        final Button cancelButton = new Button(cancelImage, ResourcesLoader.getInstance().getImage(
            "BACK_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("BACK_BUTTON_PRESSED"),
            this.getWidth() / 2 + this.backgroundWidth / 2 - cancelImage.getWidth(), this.getHeight() / 2
            + this.backgroundHeight / 2 + HighScorePanel.BUTTONDISTANCE);
        
        cancelButton.setListener(new MouseListener() {
            
            @Override
            public void actionPerformed(final Button button) {
                controller.setMainPanel();
            }
        });
        this.add(cancelButton);
        this.addScores();
        this.endGame = false;
    }
    
    /**
     * Highscore panel.
     * 
     * @param newShape
     *            shape of the panel.
     * @param container
     *            container for calling the exitgame.
     */
    public HighScorePanel(final Shape newShape, final GameContainer container) {
        super(newShape);
        this.backgroundHeight = 400;
        this.backgroundWidth = 500;
        this.addScores();
        this.endGame = true;
    }
    
    /**
     * Add the scores to the panel.
     */
    public void addScores() {
        final int lineHeight = FontUtils.FONT.getLineHeight() + 2 * HighScorePanel.BUTTONDISTANCE;
        final List<Pair<String, Long>> highscores = Highscores.getInstance().getScores();
        int yOffset = this.getHeight() / 2 - this.backgroundHeight / 2 + HighScorePanel.BUTTONDISTANCE;
        final int nameOffset = HighScorePanel.BUTTONDISTANCE + this.getWidth() / 2 - this.backgroundWidth / 2;
        
        this.add(new Label(nameOffset - HighScorePanel.BUTTONDISTANCE, yOffset, this.backgroundWidth, lineHeight, "HIGHSCORES",
            Alignment.CENTER));
        yOffset += lineHeight - HighScorePanel.BUTTONDISTANCE;
        
        for (final Pair<String, Long> score : highscores) {
            this.add(new Label(nameOffset, yOffset, this.backgroundWidth / 2 - HighScorePanel.BUTTONDISTANCE, lineHeight, score
                .getFirst()));
            this.add(new Label(nameOffset + this.backgroundWidth / 2 - HighScorePanel.BUTTONDISTANCE * 2, yOffset,
                this.backgroundWidth / 2, lineHeight,
                Long.toString(score.getSecond()), Alignment.RIGHT));
            yOffset += lineHeight;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setColor(HighScorePanel.FADE_COLOR);
        graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        graphics.setColor(HighScorePanel.BACKGROUND_COLOR);
        graphics.fillRoundRect(this.getWidth() / 2 - this.backgroundWidth / 2, this.getHeight() / 2
            - this.backgroundHeight / 2, this.backgroundWidth, this.backgroundHeight, 15);
        graphics.setColor(Color.black);
        graphics.drawRoundRect(this.getWidth() / 2 - this.backgroundWidth / 2, this.getHeight() / 2
            - this.backgroundHeight / 2, this.backgroundWidth - 1, this.backgroundHeight - 1, 15);
        super.render(container, graphics);
        if (this.endGame) {
            final UnicodeFont font = FontUtils.resizeFont(FontUtils.FONT, 30f);
            FontUtils.drawCenter(font, "Press any key to continue", 0, container.getHeight() - 250,
                container.getWidth());
        }
    }
}
