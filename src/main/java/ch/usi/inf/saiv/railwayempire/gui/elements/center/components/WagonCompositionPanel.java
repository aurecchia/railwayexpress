package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import org.newdawn.slick.geom.Shape;

import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainComponentFactory;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Display a wagon, some details about it and allows the player to add it to the current train.
 * 
 */
public class WagonCompositionPanel extends AbstractWagonPanel {
    
    /**
     * Panel displaying the current train.
     */
    private final CompositionEditorPanel trainPanel;
    
    /**
     * The button used to add the wagon to the train.
     */
    private final Button button;
    
    /**
     * Create a new wagon composition panel.
     * 
     * @param newShape
     *            The shape of the panel.
     * @param wagon
     *            The wagon to display.
     * @param trainPanel
     *            The panel holding the current train.
     */
    public WagonCompositionPanel(final Shape newShape, final IWagon wagon, final CompositionEditorPanel trainPanel) {
        super(newShape, wagon);
        
        this.trainPanel = trainPanel;
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        this.button = new Button(loader.getImage("PLUS_BUTTON"),
            loader.getImage("PLUS_BUTTON_HOVER"),
            loader.getImage("PLUS_BUTTON_PRESSED"),
            loader.getImage("PLUS_BUTTON_DISABLED"),
            0, 0);
        
        this.button.setListener(new MouseListener() {
            
            /**
             * {@inheritDoc}
             */
            @Override
            public void actionPerformed(final Button button) {
                WagonCompositionPanel.this.buttonAction();
            }
        });
        
        this.button.setLocation(this.getX() + this.getWidth() - AbstractComponentPanel.getPadding()
            - this.button.getWidth(), this.getY() + (this.getHeight() - this.button.getHeight()) / 2);
        this.add(this.button);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction() {
        final Train train = this.trainPanel.getTrain();
        if (!this.isComponentAvailable() || null == train || !train.canAddWagon()) {
            return;
        }
        train.addWagon(TrainComponentFactory.getInstance().getWagon(this.getComponent().getName()));
        Game.getInstance().getPlayer().getWareHouse().removeWagon(this.getComponent().getName());
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected Button getButton() {
        return this.button;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isComponentAvailable() {
        return this.trainPanel.getTrain() != null && this.getComponentAvailability() > 0;
    }
    
}
