package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import org.newdawn.slick.geom.Shape;

import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Player;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainComponentFactory;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Display a wagon, some details about it and allows the player to buy it.
 * 
 */
public class WagonBuyPanel extends AbstractWagonPanel {
    
    /**
     * The button used to buy the wagon.
     */
    private final Button button;
    
    /**
     * Create a new wagon buy panel.
     * 
     * @param shape
     *            The shape of the panel.
     * @param wagon
     *            The wagon to display and which can be bought.
     */
    public WagonBuyPanel(final Shape shape, final IWagon wagon) {
        super(shape, wagon);
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        this.button = new Button(loader.getImage("BUY_BUTTON"),
            loader.getImage("BUY_BUTTON_HOVER"),
            loader.getImage("BUY_BUTTON_PRESSED"),
            loader.getImage("BUY_BUTTON_DISABLED"),
            0, 0);
        
        this.button.setListener(new MouseListener() {
            
            /**
             * {@inheritDoc}
             */
            @Override
            public void actionPerformed(final Button button) {
                WagonBuyPanel.this.buttonAction();
            }
        });
        
        this.button.setLocation(this.getX() + this.getWidth() - AbstractComponentPanel.getPadding()
            - this.button.getWidth(), this.getY() + (this.getHeight() - this.button.getHeight()) / 2);
        this.add(this.button);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected Button getButton() {
        return this.button;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isComponentAvailable() {
        final int price = TrainComponentFactory.getInstance().getWagon(this.getComponent().getName()).getCost();
        return Game.getInstance().getPlayer().canAfford(price);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction() {
        final Player player = Game.getInstance().getPlayer();
        final int price = TrainComponentFactory.getInstance().getWagon(this.getComponent().getName()).getCost();
        if (player.canAfford(price)) {
            player.subtractMoney(price);
            player.getWareHouse().addWagon(this.getComponent().getName());
        }
    }
    
}
