package ch.usi.inf.saiv.railwayempire.controllers;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import ch.usi.inf.saiv.railwayempire.gui.GameState;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.menu.CreditPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.menu.HighScorePanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.menu.NewGamePanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.menu.SaveLoadPanel;
import ch.usi.inf.saiv.railwayempire.gui.states.MainMenuState;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;

/**
 * Controller for the main menu.
 * 
 */
public class MainMenuController extends AbstractGameController {

    /**
     * Slick state
     */
    private final MainMenuState mainMenuState;

    /**
     * Default panel.
     */
    private final SimplePanel mainPanel;

    /**
     * Panel for a new game.
     */
    private final NewGamePanel newGamePanel;

    /**
     * Panel for the load.
     */
    private final SaveLoadPanel loadPanel;

    /**
     * Panel for the credits.
     */
    private final CreditPanel creditPanel;

    /**
     * Panel for the highscores.
     */
    private final HighScorePanel highScorePanel;

    /**
     * Construct for the controller of the main menu.
     * 
     * @param mainMenuState
     *            slick state of the main menu.
     * @param container
     *            slick container.
     * @param game
     *            slick instace of the game.
     */
    public MainMenuController(final MainMenuState mainMenuState, final GameContainer container,
            final StateBasedGame game) {
        super(container, game);
        this.mainMenuState = mainMenuState;

        this.mainPanel = new SimplePanel(new Rectangle(SystemConstants.ZERO, SystemConstants.ZERO,
                container.getWidth(), container.getHeight()));

        final Image newGameButtonImage = ResourcesLoader.getInstance().getImage("MAIN_NEW_BUTTON");
        final Image loadGameButtonImage = ResourcesLoader.getInstance().getImage("MAIN_LOAD_BUTTON");
        final Image highScoreButtonImage = ResourcesLoader.getInstance().getImage("MAIN_HIGHSCORES_BUTTON");
        final Image creditButtonImage = ResourcesLoader.getInstance().getImage("MAIN_CREDITS_BUTTON");
        final Image exitGameButtonImage = ResourcesLoader.getInstance().getImage("MAIN_EXIT_BUTTON");

        final int buttonY = this.mainPanel.getHeight() / SystemConstants.TWO;

        int rateoY = newGameButtonImage.getHeight() * 2;

        final Button newGameButton = new Button(newGameButtonImage, ResourcesLoader.getInstance().getImage(
                "MAIN_NEW_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("MAIN_NEW_BUTTON"),
                this.mainPanel.getWidth() / SystemConstants.TWO - newGameButtonImage.getWidth() / SystemConstants.TWO,
                buttonY - rateoY) {
        };

        rateoY -= newGameButtonImage.getHeight();

        final Button loadGameButton = new Button(loadGameButtonImage, ResourcesLoader.getInstance().getImage(
                "MAIN_LOAD_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("MAIN_LOAD_BUTTON"),
                this.mainPanel.getWidth() / SystemConstants.TWO - loadGameButtonImage.getWidth() / SystemConstants.TWO,
                buttonY - rateoY) {
        };

        rateoY -= newGameButtonImage.getHeight();

        final Button highScoreButton = new Button(highScoreButtonImage, ResourcesLoader.getInstance().getImage(
                "MAIN_HIGHSCORES_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("MAIN_HIGHSCORES_BUTTON"),
                this.mainPanel.getWidth() / SystemConstants.TWO - highScoreButtonImage.getWidth() / SystemConstants.TWO,
                buttonY - rateoY) {
        };

        rateoY -= newGameButtonImage.getHeight();

        final Button creditButton = new Button(creditButtonImage, ResourcesLoader.getInstance().getImage(
                "MAIN_CREDITS_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("MAIN_CREDITS_BUTTON"),
                this.mainPanel.getWidth() / SystemConstants.TWO - creditButtonImage.getWidth() / SystemConstants.TWO,
                buttonY - rateoY) {
        };

        rateoY -= newGameButtonImage.getHeight();

        final Button exitGameButton = new Button(exitGameButtonImage, ResourcesLoader.getInstance().getImage(
                "MAIN_EXIT_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("MAIN_EXIT_BUTTON"),
                this.mainPanel.getWidth() / SystemConstants.TWO - exitGameButtonImage.getWidth() / SystemConstants.TWO,
                buttonY - rateoY) {
        };

        newGameButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                MainMenuController.this.setNewGamePanel();
            }
        });
        loadGameButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                MainMenuController.this.setLoadPanel();
            }
        });
        highScoreButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                MainMenuController.this.setHighscorePanel();
            }
        });
        creditButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                MainMenuController.this.setCrediPanel();
            }
        });
        exitGameButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                MainMenuController.this.exitGame();
            }
        });

        this.mainPanel.add(newGameButton);
        this.mainPanel.add(loadGameButton);
        this.mainPanel.add(highScoreButton);
        this.mainPanel.add(creditButton);
        this.mainPanel.add(exitGameButton);

        this.loadPanel = new SaveLoadPanel(new Rectangle(SystemConstants.ZERO, SystemConstants.ZERO, container.getWidth(),
                container.getHeight()), this, 3, container, false);

        this.newGamePanel = new NewGamePanel(new Rectangle(SystemConstants.ZERO, SystemConstants.ZERO,
                container.getWidth(), container.getHeight()), this, container);

        this.creditPanel = new CreditPanel(new Rectangle(SystemConstants.ZERO, SystemConstants.ZERO,
                container.getWidth(), container.getHeight()), this);

        this.highScorePanel = new HighScorePanel(new Rectangle(SystemConstants.ZERO, SystemConstants.ZERO,
                container.getWidth(), container.getHeight()), this);
        this.setMainPanel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setMainPanel() {
        this.mainMenuState.setCurrentPanel(this.mainPanel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLoadPanel() {
        this.loadPanel.refresh();
        this.mainMenuState.setCurrentPanel(this.loadPanel);
    }

    /**
     * Show the new game panel.
     */
    public void setNewGamePanel() {
        this.mainMenuState.setCurrentPanel(this.newGamePanel);
    }

    /**
     * Show the credit panel.
     */
    public void setCrediPanel() {
        this.mainMenuState.setCurrentPanel(this.creditPanel);
    }

    /**
     * Show the highscores panel.
     */
    public void setHighscorePanel() {
        this.mainMenuState.setCurrentPanel(this.highScorePanel);
    }


    /**
     * Start a new game
     */
    public void newGame() {
        Game.getInstance().generateWorld(new WorldSettings());
        this.game.enterState(GameState.GAMEPLAY.ordinal());
    }

}
