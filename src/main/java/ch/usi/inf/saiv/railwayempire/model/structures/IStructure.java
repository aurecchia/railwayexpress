package ch.usi.inf.saiv.railwayempire.model.structures;


import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;


/**
 * Interface for structures.
 */
public interface IStructure {
    
    /**
     * Return the x coordinate in the grid.
     *
     * @return x coordinate.
     */
    int getX();
    
    /**
     * Return the y coordinate in the grid.
     *
     * @return y coordinate.
     */
    int getY();
    
    /**
     * Returns the structure as an production site, or null if structure is not a production site.
     *
     * @return Extraction site-d structure.
     */
    IProductionSite asProductionSite();
    
    /**
     * Returns the structure as a Station, or null if structure is not a Station.
     *
     * @return structure as station.
     */
    Station asStation();
    
    /**
     * Returns the structure as a Track, or null if structure is not a track.
     *
     * @return structure as track.
     */
    Track asTrack();
    
    /**
     * Returns the texture of the structure.
     *
     * @return String name of the texture.
     */
    String getTexture();
    
    /**
     * Get structure type.
     *
     * @return StructureType type of the cell.
     */
    StructureTypes getStructureType();
    
    /**
     * Method that check if a structure can be removed or not.
     *
     * @return boolean representing structure removability.
     */
    boolean isRemovable();
    
    /**
     * Get description text on hover.
     * 
     * @return text.
     */
    String getHoverText();
}
