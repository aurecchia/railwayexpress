/**
 * Provides the functionality of having different states of the game which provide different actions.
 *
 * NormalMode - play mode which does not provide any additional options. It is possible to
 * select production sites, stations, locomotives and wagons which results in additional information
 * being displayed in bottom info utility panel.
 *
 * StationCreationMode - allows player to create station in the world. During this mode, a player
 * will be informed only with information significant to station creation.
 *
 *  TrackCreationMode - allows player to create tracks in the world. While this mode is active,
 *  a player will be informed only with information significant to track creation. It is possible to create
 *  tracks by clicking specially designed buttons or by clicking in the world and dragging the mouse.
 *
 *  BuildingCreationMode - allows player to create buildings in the world. While this mode is active,
 *  a player will be informed only with information significant to building creation.
 */
package ch.usi.inf.saiv.railwayempire.gui.modes;