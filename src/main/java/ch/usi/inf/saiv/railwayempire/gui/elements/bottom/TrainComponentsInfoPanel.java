package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Shape;

/**
 * Info panel for the train components view.
 */
public final class TrainComponentsInfoPanel extends AbstractInfoPanel {
    
    /**
     * Constructor.
     * 
     * @param shape
     *            shape of panel.
     */
    public TrainComponentsInfoPanel(final Shape shape) {
        super(shape);
    }
}
